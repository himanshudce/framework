const { plugins, util } = require('@citation-js/core')
const { parse: parseDate } = require('@citation-js/date')
const { parse: parseName } = require('@citation-js/name')

function createCdliNumber (id) {
    return 'P' + id.toString().padStart(6, '0')
}

function parseCdliAuthor ({ author: { id, author } }) {
    if (id === 820) {
        return { literal: 'Cuneiform Digital Library Iniative (CDLI)' }
    } else {
        return parseName(author)
    }
}

const artifactTranslator = new util.Translator([
    { source: null, target: 'type', convert: { toTarget () { return 'webpage' } } },
    {
        source: 'designation',
        target: 'title',
        convert: { toTarget (designation) { return `${designation} artifact entry` } }
    },
    {
        source: 'credits',
        target: 'author',
        convert: {
            toTarget (credits) {
                return credits
                    .filter(({ author_id }, index) => credits.findIndex(credit => credit.author_id === author_id) === index)
                    .map(parseCdliAuthor)
            }
        }
    },
    {
        source: 'modified',
        target: 'issued',
        convert: { toTarget: parseDate }
    },
    {
        source: null,
        target: 'accessed',
        convert: {
            toTarget () { return parseDate(Date.now()) }
        }
    },
    {
        source: 'inscriptions',
        target: 'abstract',
        convert: {
            toTarget ([inscription]) {
                return inscription ? inscription.transliteration_clean : undefined
            }
        }
    },
    {
        source: 'id',
        target: ['id', 'number', 'URL'],
        convert: {
            toTarget (id) {
                const number = createCdliNumber(id)
                return [
                    number,
                    number,
                    `https://cdli.ucla.edu/${number}`
                ]
            }
        }
    },
    {
        source: 'languages',
        target: 'language',
        convert: {
            toTarget ([language]) {
                return language ? language.language : undefined
            }
        }
    }
])

plugins.add('@cdli', {
    input: {
        '@cdli/artifact': {
            parse (artifact) {
                return artifactTranslator.convertToTarget(artifact)
            },
            parseType: {
                dataType: 'SimpleObject',
                propertyConstraint: {
                    props: ['designation', 'inscriptions']
                }
            }
        },
        '@cdli/publication': {
            parse (publication) {
                const output = {
                    type: publication.entry_type ? publication.entry_type.label : 'misc',
                    label: publication.bibtexkey || publication.id,
                    properties: {}
                }

                if (publication.book_title) {
                    publication.booktitle = publication.book_title
                }

                for (const prop in publication) {
                    if (publication[prop] === null || publication[prop].length === 0) {
                        continue
                    }

                    output.properties[prop] = (function (value) {
                        switch (prop) {
                            case 'journal':
                                return value.journal
                            case 'authors':
                                return value.map(author => author.author).join(' and ')
                            default:
                                return value
                        }
                    })(publication[prop])
                }

                return output
            },
            parseType: {
                dataType: 'SimpleObject',
                propertyConstraint: {
                    props: ['bibtexkey', 'entry_type_id']
                }
            }
        }
    }
})
