const Cite = require('@citation-js/core')
require('@citation-js/plugin-bibtex')
require('@citation-js/plugin-csl')
require('@citation-js/plugin-ris')
require('./plugin-cdli')

// Disable URL requests
Cite.plugins.input.removeDataParser('@else/url', true)
Cite.plugins.input.removeDataParser('@else/url', false)

const express = require('express')
const app = express()

// Option parsing
function addOption (options, [key, ...path], value) {
    if (path.length === 0) {
        options[key] = value
    } else if (key in options) {
        addOption(options[key], path, value)
    } else {
        addOption(options[key] = {}, path, value)
    }
}

function createOptions (query) {
    const options = {}
    for (const param in query) {
        const path = param.split('.')
        addOption(options, path, query[param])
    }
    return options
}

// Body parsing
app.use(express.text())
app.use(express.json())

// Parse and format data
app.post('/format/:format', ({ body, params: { format }, query }, res) => {
    if (!Cite.plugins.output.has(format)) {
        res.sendStatus(406) // Not Acceptable
    } else if (Cite.plugins.input.type(body) === '@invalid') {
        res.sendStatus(415) // Unsupported Media Type
    } else {
        let data
        try {
            data = new Cite.Cite(body, {
                generateGraph: false
            })
            try {
                res.send(data.format(format, createOptions(query)))
            } catch (e) {
                res.sendStatus(500)
            }
        } catch (e) {
            console.log(e)
            res.sendStatus(415)
        }
    }
})

app.get('/formats/input', (_, res) => res.send(Cite.plugins.input.list()))
app.get('/formats/output', (_, res) => res.send(Cite.plugins.output.list()))

module.exports = {
    app,
    message: `Running Citation.js v${Cite.version}...`
}
