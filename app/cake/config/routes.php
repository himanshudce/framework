<?php
/**
 * Routes configuration
 *
 * In this file, you set up routes to your controllers and their actions.
 * Routes are very important mechanism that allows you to freely connect
 * different URLs to chosen controllers and their actions (functions).
 *
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link          https://cakephp.org CakePHP(tm) Project
 * @license       https://opensource.org/licenses/mit-license.php MIT License
 */

use Cake\Core\Plugin;
use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

/**
 * The default class to use for all routes
 *
 * The following route classes are supplied with CakePHP and are appropriate
 * to set as the default:
 *
 * - Route
 * - InflectedRoute
 * - DashedRoute
 *
 * If no call is made to `Router::defaultRouteClass()`, the class used is
 * `Route` (`Cake\Routing\Route\Route`)
 *
 * Note that `Route` does not do any inflections on URLs which will result in
 * inconsistently cased URLs when used with `:plugin`, `:controller` and
 * `:action` markers.
 *
 */
Router::defaultRouteClass(DashedRoute::class);

Router::scope('/', function (RouteBuilder $routes) {
    /**
     * Here, we are connecting '/' (base path) to a controller called 'Pages',
     * its action called 'display', and we pass a param to select the view file
     * to use (in this case, src/Template/Pages/home.ctp)...
     */
    //$routes->connect('/', ['controller' => 'Pages', 'action' => 'display', 'home']);

    /**
     * ...and connect the rest of 'Pages' controller's URLs.
     */
    // Any controller will interpret the variable as id run through view action.

    // Router::scope('/abbreviations', function ($routes) {

    //     $routes->connect('/view/*', ['controller' => 'Abbreviations', 'action' => 'view']);
    //     $routes->fallbacks('InflectedRoute');
    // });

    $routes->addExtensions([
        'pdf',
        'json',

        // Linked data
        'xml',
        'rdf',
        'ttl',
        'nt',
        'jsonld',

        // Tables
        'csv',
        'tsv',
        'xlsx',

        // Bibliographies
        'bib',
        'ris'
    ]);

    // Permalinks
    $routes->connect('/:id', ['controller' => 'Artifacts', 'action' => 'resolve'])
        ->setPatterns(['id' => '[PQS]\d{6}|S\d{6}\.\d'])
        ->setPass(['id']);

    // Admin prefix
    Router::prefix('admin', function ($routes) {
        // Pages for each controller
        $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'InflectedRoute']);
        $routes->connect('/:controller/edit/*', ['action' => 'edit']);
        $routes->connect('/:controller/add/*', ['action' => 'add']);
        $routes->connect('/:controller/delete/*', ['action' => 'delete']);
        $routes->connect('/:controller/export/*', ['action' => 'export']);
        $routes->connect('/:controller/*', ['action' => 'view']);

        // All Users Profile Edit
        $routes->connect('/users/:username/edit', ['controller' => 'Users', 'action' => 'edit'], ['pass' => ['username']]);
        $routes->connect('/publications/autocomplete', ['controller' => 'Publications', 'action' => 'autocomplete']);
        // Dashboard
        $routes->connect('/dashboard', ['controller' => 'Home', 'action' => 'dashboard']);

        // Routes for Admin journals dashboard.
        $routes->connect('/articles/add/CDLN', ['controller' => 'Articles', 'action' => 'add_cdln']);
        $routes->connect('/articles/add/CDLP', ['controller' => 'Articles', 'action' => 'add_cdlp']);
        $routes->connect('/articles/edit/CDLN/:article', ['controller' => 'Articles', 'action' => 'edit_cdln'])
        ->setPass(['article']);
        $routes->connect('/articles/edit/CDLP/:article', ['controller' => 'Articles', 'action' => 'edit_cdlp'])
        ->setPass(['article']);

        // Routes for Journals display at admin dashboard.
        $routes->connect('/articles/cdln', ['controller' => 'Articles', 'action' => 'index_cdln']);
        $routes->connect('/articles/cdlp', ['controller' => 'Articles', 'action' => 'index_cdlp']);
        $routes->connect('/articles/cdln/view/:cdln', ['controller' => 'Articles', 'action' => 'view_cdln']);
        $routes->connect('/articles/cdln/view/:cdln/web', ['controller' => 'Articles', 'action' => 'view_cdln_web']);
        $routes->connect('/articles/cdlp/view/:cdlp', ['controller' => 'Articles', 'action' => 'view_cdlp']);

        $routes->connect('/articles/add/:article/upload', ['controller' => 'Articles', 'action' => 'upload_article_pdf'])
        ->setPass(['article']);
        $routes->connect('/articles/delete/:article', ['controller' => 'Articles', 'action' => 'delete_article'])
        ->setPass(['article']);

        // Routes for Admin Author Ajax.
        $routes->connect('/authors/search/:author', ['controller' => 'Authors', 'action' => 'author_search_ajax'])
        ->setPass(['author']);
        $routes->connect('/authors/add/:author', ['controller' => 'Authors', 'action' => 'add_author_ajax'])
        ->setPass(['author']);

        $routes->fallbacks('DashedRoute');
    });

    Router::prefix('editor', function ($routes) {
        // Because you are in the admin scope,
        // you do not need to include the /admin prefix
        // or the admin route element.
        $routes->fallbacks('DashedRoute');
    });

    // Browse
    $routes->connect('/artifacts/browse', ['controller' => 'Artifacts', 'action' => 'browse']);
    $routes->connect('/artifacts/auth-image/*', ['controller' => 'Artifacts', 'action' => 'authImage']);

    // Routes for Journals.
    $routes->connect('/articles/cdln', ['controller' => 'Articles', 'action' => 'index_cdln']);
    $routes->connect('/articles/cdlp', ['controller' => 'Articles', 'action' => 'index_cdlp']);
    $routes->connect('/articles/cdln/view/:cdln', ['controller' => 'Articles', 'action' => 'view_cdln']);
    $routes->connect('/articles/cdln/view/:cdln/web', ['controller' => 'Articles', 'action' => 'view_cdln_web']);
    $routes->connect('/articles/cdlp/view/:cdlp', ['controller' => 'Articles', 'action' => 'view_cdlp']);

    // Visualization routes
    $routes->connect('/AdvancedSearch/getUpdatedLineChartData', ['controller' => 'AdvancedSearch', 'action' => 'getUpdatedLineChartData']);
    $routes->connect('/stats/:table', ['controller' => 'Stats', 'action' => 'index'], ['pass' => ['table']]);
    $routes->connect('/viz/:action', ['controller' => 'Viz']);
    $routes->connect('/heatmap/post', ['controller' => 'Heatmap', 'action' => 'post']);

    // For User Controller
    $routes->connect('/users/profile/*', ['controller' => 'Users', 'action' => 'profile']);
    $routes->connect('/login', ['controller' => 'Users', 'action' => 'login']);
    $routes->connect('/register', ['controller' => 'Users', 'action' => 'register']);

    // Documentation
    $routes->connect('/docs/*', ['controller' => 'Pages', 'action' => 'display']);

    // Pages for each controller
    $routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'InflectedRoute']);
    $routes->connect('/:controller/:id/:format', ['action' => 'view'])->setPass(['id']);
    $routes->connect('/:controller/*', ['action' => 'view']);

    // Home page
    $routes->connect('/', ['controller' => 'Home']);

    // Browse
    $routes->connect('/browse', ['controller' => 'Home', 'action' => 'browse']);

    // Forgot Controller
    $routes->connect('/forgot/:type', ['controller' => 'Forgot', 'action' => 'index'], ['pass' => ['type']]);
    // Password Reset (New Password Page)
    $routes->connect('/forgot/password/reset/:token', ['controller' => 'Forgot', 'action' => 'password'], ['pass' => ['token']]);

    /**
     * Connect catchall routes for all controllers.
     *
     * Using the argument `DashedRoute`, the `fallbacks` method is a shortcut for
     *    `$routes->connect('/:controller', ['action' => 'index'], ['routeClass' => 'DashedRoute']);`
     *    `$routes->connect('/:controller/:action/*', [], ['routeClass' => 'DashedRoute']);`
     *
     * Any route class can be used with this method, such as:
     * - DashedRoute
     * - InflectedRoute
     * - Route
     * - Or your own route class
     *
     * You can remove these routes once you've connected the
     * routes you want in your application.
    */
    //Set up router for REST API
    $routes->resources('Artifacts');

    $routes->fallbacks('DashedRoute');
});

/**
 * Load all plugin routes. See the Plugin documentation on
 * how to customize the loading of plugin routes.
 */

// Plugin::routes();            // Raises Deprecated Warning as no longer requirement to call `Plugin::routes()` after upgrading to use Http\Server
