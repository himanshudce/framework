<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsExternalResource[]|\Cake\Collection\CollectionInterface $artifactsExternalResources
 */
?>


<h3 class="display-4 pt-3"><?= __('Artifacts External Resources') ?></h3>

<table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th scope="col"><?= $this->Paginator->sort('artifact_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('external_resource_id') ?></th>
            <th scope="col"><?= $this->Paginator->sort('external_resource_key') ?></th>
            <!-- <th scope="col"><?= __('Actions') ?></th> -->
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsExternalResources as $artifactsExternalResource): ?>
        <tr>
            <td><?= $artifactsExternalResource->has('artifact') ? $this->Html->link($artifactsExternalResource->artifact->designation, ['controller' => 'Artifacts', 'action' => 'view', $artifactsExternalResource->artifact->id]) : '' ?></td>
            <td><?= $artifactsExternalResource->has('external_resource') ? $this->Html->link($artifactsExternalResource->external_resource->external_resource, ['controller' => 'ExternalResources', 'action' => 'view', $artifactsExternalResource->external_resource->id]) : '' ?></td>
            <td><?= h($artifactsExternalResource->external_resource_key) ?></td>
            <!-- <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-search']),
                        ['action' => 'view', $artifactsExternalResource->id],
                        ['escape' => false, 'class' => 'btn btn-outline-primary m-1', 'title' => 'View']) ?>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $artifactsExternalResource->id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $artifactsExternalResource->id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsExternalResource->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td> -->
        </tr>
        <?php endforeach; ?>
    </tbody>
</table>

<div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
</div>

