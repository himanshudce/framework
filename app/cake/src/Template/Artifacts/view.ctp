<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Artifact $artifact
 */

$CDLI_NO = $artifact->getCdliNumber();
$this->assign('title', $artifact->designation . ' (' . $CDLI_NO . ')');
$this->assign('image', 'https://cdli.ucla.edu/dl/photo/' . $CDLI_NO . '.jpg');

$exportTypes = ['csv','xlsx','tsv','json','xml'];
?>

<main id="artifact">
    <a id="back" href="#" onclick="history.back()"><i class="fa fa-chevron-left"></i> Back to Search Results</a>
    <h1 class="display-3 header-txt"><?= h($artifact->designation) ?> (<?= h($CDLI_NO) ?>)</h1>
    <h2 class="my-4 artifact-desc">
        <?php if (!empty($artifact->genres)): ?>
            <?= h($artifact->genres[0]->genre) ?>,
        <?php endif; ?>
        <?php if (!empty($artifact->artifact_type)): ?>
            <?= h($artifact->artifact_type->artifact_type) ?>,
        <?php endif; ?>
        <?php if (!empty($artifact->provenience)): ?>
            <?= h($artifact->provenience->provenience) ?>
        <?php endif; ?>
        <?php if (!empty($artifact->period)): ?>
            in <?= h($artifact->period->period) ?>
        <?php endif; ?>
        <?php if (!empty($artifact->collections)): ?>
            and kept at <?= h($artifact->collections[0]->collection) ?>
        <?php endif; ?>
    </h2>

    <div class="export-options">
        <div class="dropdown">
            <a class="dropdown-toggle export-as-dd" id="exportDropDown" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Export artifact
            </a>
            <div class="dropdown-menu" aria-labelledby="exportDropDown">
                <?php foreach ($exportTypes as $format): ?>
                    <?= $this->Html->link('As '.$format, [
                        'controller' => 'Artifacts',
                        'action' => 'view',
                        $artifact->id,
                        '_ext' => $format,
                    ], [
                        'class' => 'dropdown-item'
                    ])?>
                <?php endforeach;?>
            </div>
        </div>
    </div>

    <?= $this->cell('ArtifactView', [$artifact->id]) ?>
</main>
<?= $this->Scroll->toTop();?>
