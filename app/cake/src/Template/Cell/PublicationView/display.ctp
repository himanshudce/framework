<div class="row justify-content-md-center">
    <div class="col-lg boxed">
        <legend class="capital-heading"><?= __('Publication Information:') ?></legend>
        <table cellspacing=10 cellpadding=10 >
            <tr>
                <td>Designation:</td>
                <td><?= isset($publication->designation) ? h($publication->designation): 'Designation not available' ?><br></td>
            </tr>
            <tr>
                <td colspan=2><?= $this->Html->link('View Publication', [
                    'prefix' => false,
                    'controller' => 'Publications',
                    'action' => 'view',
                    $publication->id
                ], [
                    'class' => 'btn btn-action'
                ]) ?>
                </td>
            </tr>
        </table>
    </div>
</div>
