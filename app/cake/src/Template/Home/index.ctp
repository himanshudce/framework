<?php
    $searchCategory = [
        // $category =. $placeholder
        'Keyword' => 'Keywords',
        'Publication' => 'Publications',
        'Collection' => 'Collections',
        'Provenience' => 'Proveniences',
        'Period' => 'Periods',
        'Inscription' => 'Inscriptions',
        'ID' => 'ID\'s'  
    ];
echo $this->Html->script('searchResult.js', ['defer' => true]);
?>

<div class="container">
    <div>
        <h1 class="display-3 text-left header-txt">The CDLI Collection</h1>
        <p class="text-left page-summary-text mt-4">By making the form and content of cuneiform texts available online, the CDLI is opening pathways to the rich historical tradition of the ancient Middle East. In close collaboration with researchers, museums and an engaged public, the project seeks to unharness the extraordinary content of these earliest witnesses to our shared world heritage.</p>
    </div>

    <?= $this->Form->create("", [
            'type' => 'GET',
            'url' => [
                'controller' => 'Search',
                'action' => 'index'
            ]
        ]) ?>
    <div class="mt-5">
        <div id="dynamic_field">
            <div>
                
                <!-- search layout when no script is enabled -->
                <noscript>
                    <style>
                        .default-search-block, 
                        .default-add-search-btn{
                            display:none;
                        }
                    </style>
                    <div id="dynamic_field">
                        <div>
                            <div class="rectangle-2 container p-3"> 
                                <div class="search-page-grid" id="2">
                                    <label hidden="" for="input1">Query</label>
                                    <input type="text" id="input1" name="Keyword" placeholder="Search for publications, provenience, collection no." aria-label="" aria-describedby="">
                                    <label hidden="" for="1">Category</label>
                                    <select class="form-group mb-0" id="1">
                                    <?php foreach ($searchCategory as $category=>$placeholder): ?>
                                                <option value="<?= $category ?>">
                                                    <?= $placeholder ?>
                                                </option>
                                    <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    <?php for ($i = 1; $i<=2; $i++):?>
                        <div id="row<?=$i?>">
                            <div class="container rectangle-23 p-3">
                                <div class="align-items-baseline">
                                    <select type="dropdown" name="and" id="<?=$i?>" class="mb-3 mt-0 cdli-btn-light btn-and mr-3 float-left">
                                        <option>AND</option>
                                        <option>OR</option>
                                    </select>
                                    <div class="search-page-grid w-100-sm">
                                        <input type="text" class="form-control" id="input<?=$i?>" placeholder="Search for publications, provenience, collection no.">
                                        <select class="form-group mb-0" id="<?=$i?>">
                                            <?php foreach ($searchCategory as $category=>$placeholder): ?>
                                                    <option value="<?= $category ?>">
                                                        <?= $placeholder ?>
                                                    </option>
                                            <?php endforeach; ?>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php endfor; ?>
                </noscript>

                <div class="rectangle-2 container p-3 default-search-block"> 
                    <div class="search-page-grid" id="2">
                        <label hidden for="input1">Query</label>
                        <input 
                            type="text" 
                            id="input1" 
                            name="Keyword"
                            placeholder="Search for publications, provenience, collection no." 
                            aria-label="Search"
                            required = true
                        />
                        <label hidden for="1">Category</label>
                        <select class="form-group mb-0" id="1">
                            <?php foreach ($searchCategory as $category=>$placeholder): ?>
                                        <option value="<?= $category ?>">
                                            <?= $placeholder ?>
                                        </option>
                            <?php endforeach; ?>
                        </select>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="container d-flex cdli-btn-group px-0 search-add-field-group">
        <?= $this->Form->button('Search', ['type' => 'submit', 'class' => 'btn cdli-btn-blue']); ?>
            <button type="button" name="add" id="add" class="btn cdli-btn-light default-add-search-btn">
                <span class="fa fa-plus-circle plus-icon"></span>Add search field
            </button>
            <a class="d-none d-lg-block search-links mr-5" href="#">Search settings</a>
            <a class="d-none d-lg-block search-links mr-5" href="/AdvancedSearch">Advanced search</a>
        <?= $this->Form->end()?>
    </div>    
    <hr class="line mt-5"/>

    <p class="text-left display-4 section-title">Highlights</p>
    <div class="row mt-5">
        <?php foreach ($highlights as $highlight):
            $image = $this->ArtifactImages->getMainImage($highlight['artifact_id']);
            ?>
            <div class='card bs4-cdli-card col-sm-12 col-md-6 col-lg-4 mb-5'>
                <img
                src = '<?=$image['thumbnail']?>'
                alt = 'Highlight <?= $highlight['title'].' '.$highlight['image_type']?> '
                class = 'card-img-top'>
                <div class='card-body'>
                    <h5 class='card-title'>
                        <?= $this->Html->link(strlen($highlight['title']) > 38 ? substr($highlight['title'], 0, 38)."..." : $highlight['title'], ['controller' => 'postings', 'action' => 'view', $highlight['id']]);?>
                    </h5>
                    <p class='card-text'>
                        <?=
                        strlen(strip_tags($highlight['body'])) > 100 ? substr(strip_tags($highlight['body']), 0, 100)."..." : strip_tags($highlight['body'])
                        // use above to trim extra card-length
                        ?>
                    </p>
                </div>
            </div>
        <?php endforeach;?>
    </div>

    <p class="text-left display-4 section-title ">News</p>
    <div class="row mt-5">
        <?php foreach ($newsarticles as $news):
            $image = $this->ArtifactImages->getMainImage($news['artifact_id']);
            ?>
            <div class='card bs4-cdli-card col-sm-12 col-md-6 col-lg-4 mb-5'>
                <img
                src = '<?=$image['thumbnail']?>'
                alt = 'News <?= $news['title'].' '.$news['image_type']?> '
                class = 'card-img-top'>
                <div class='card-body'>
                    <h5 class='card-title'>
                        <?= $this->Html->link(strlen($news['title']) > 38 ? substr($news['title'], 0, 38)."..." : $news['title'], ['controller' => 'postings', 'action' => 'view', $news['id']]);?>
                    </h5>
                    <p class="date-style"><?=date_format($news['publish_start'], 'Y-m-d')?></p>
                    <p class='card-text'>

                        <?=
                        strlen(strip_tags($news['body'])) > 100 ? substr(strip_tags($news['body']), 0, 100)."..." : strip_tags($news['body'])
                        // use above to trim extra card-length
                        ?>
                    </p>
                </div>
            </div>
        <?php endforeach;?>
    </div>
</div>
<?= $this->Scroll->toTop()?>
