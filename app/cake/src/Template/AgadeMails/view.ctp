<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\AgadeMail $agademail
 */
?>

<div align="center" style="display:block">

    <table>
        <th>
            <td>
                <?= $this->Form->create('CdliTags', ['controller' => 'AgadeMails','action' => $prev_id.'/'.$filter_tag]); ?>
                <?= $this->Form->hidden('filter_tag', ['value' => $filter_tag]); ?>
                <?= $this->Form->button(__('Previous'), ['disabled' => ($prev_id == null), 'class' => 'btn btn-primary ']); ?>
                <?= $this->Form->end(); ?>
            </td>
            <td>
                <?= $this->Form->create('CdliTags', ['controller' => 'AgadeMails','action' => $next_id.'/'.$filter_tag]); ?>
                <?= $this->Form->hidden('filter_tag', ['value' => $filter_tag]); ?>
                <?= $this->Form->button(__('Next'), ['disabled' => ($next_id == null), 'class' => 'btn btn-primary '.(($next_id == null) ? 'disabled':'')]); ?>
                <?= $this->Form->end(); ?>
            </td>
        </th>
    </table>
    
</div>

<div class="row justify-content-md-center">

    <div class="boxed">
        <div class="capital-heading"><?= __('View Email') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Title') ?></th>
                    <td><?= h($agademail->title) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Date') ?></th>
                    <td><?= h($agademail->date) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Category') ?></th>
                    <td><?= h($agademail->category) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('CDLI Tag') ?></th>
                    <td><?= h($agademail->cdli_tag) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Content') ?></th>
                    <!-- Formatting for retaining new line characters and displaying clickable links and emails -->
                    <td>
                        <?php $string = nl2br( str_replace('>',' ', str_replace('<',' ',$agademail->content)) );
                        $url = '~(?:(https?)://([^\s<]+)|(www\.[^\s<]+?\.[^\s<]+))(?<![\.,:])~i'; 
                        $string = preg_replace($url, '<a href="$0" target="_blank" title="$0">$0</a>', $string);
                        $search  = array('/<p>__<\/p>/', '/([a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4})/');
                        $replace = array('<hr />', '<a href="mailto:$1">$1</a>');
                        $processed_string = preg_replace($search, $replace, $string);
                        echo $processed_string; ?>
                    </td>
                </tr>
            </tbody>
        </table>
        
        <?= $this->Form->create('CdliTags', ['controller' => 'AgadeMails','action' => 'index']); ?>

        <?= $this->Form->hidden('filter_tag', ['value' => $filter_tag]); ?>
        <p align="center"><?= $this->Form->button(__('Go Back'),['class' => 'btn btn-primary']); ?></p>
        <?= $this->Form->end(); ?>
        

</div>


