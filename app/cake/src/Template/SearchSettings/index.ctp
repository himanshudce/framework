<div class="ads searchSettings">
    <?= $this->Form->create("", ['type'=>'post']) ?>
		<div class="">
			<div class="d-flex align-items-center justify-content-between mb-3">
				<h1 class="display-3 text-left advanced-title">Search Settings</h4>
				<?= $this->Form->button('Save', [
						'type' => 'submit',
						'class' => 'search-btn btn d-none d-md-block px-5'
					]
				); ?>
			</div>
		</div>

		<div class="layout-grid text-left">
			<div>
				<div class="darken">
					<p class="form-title mb-0">Search Result options</p>
					<span class="subscript-txt">Number of search results per page</span>

					<div class="mt-4">
						<label>Results per page:
							<?= $this->Form->select('PageSize',
								$SearchSettingOption['PageSize'], [
									'value' => $searchSettings['PageSize'],
									'empty' => false,
									'class' => 'ss-sort-select mb-0'
								]
							); ?>
						</label>
					</div>

				</div>

				<div class="darken ads-space-top">
					<p class="form-title mb-0">filter options</p>
					<span class="subscript-txt">Add/remove filter options in sidebar</span>

					<div class="mt-4">
						<p class="ss-filter-head">Object data</p>
						<fieldset>
							<legend class="d-none">Object data list</legend>
							<?= $this->Form->select('filter:object',
								$SearchSettingOption['Filter']['Object'], [
									'multiple' => 'checkbox',
									'value' => $searchSettings['filter:object']
								]
							); ?>
						</fieldset>
					</div>

					<div class="mt-4">
						<p class="ss-filter-head">Textual data</p>
						<fieldset>
							<legend class="d-none">Textual data list</legend>
							<?= $this->Form->select('filter:textual',
								$SearchSettingOption['Filter']['Textual'], [
									'multiple' => 'checkbox',
									'value' => $searchSettings['filter:textual']
								]
							); ?>
						</fieldset>
					</div>

					<div class="mt-4">
						<p class="ss-filter-head">Publication data</p>
						<fieldset>
							<legend class="d-none">Publication data list</legend>
							<?= $this->Form->select('filter:publication',
								$SearchSettingOption['Filter']['Publication'], [
									'multiple' => 'checkbox',
									'value' => $searchSettings['filter:publication']
								]
							); ?>
						</fieldset>
					</div>
					
				</div>

				<div class="darken ads-space-top">
					<p class="form-title mb-0">Compact view options</p>
					<span class="subscript-txt">Add/remove elements in compact view</span>

					<div>
						<p class="ss-filter-head mt-4">
						Show/hide filter sidebar (compact)
						<p>
						
						<fieldset>
							<legend class="d-none">Sidebar visibility(compactview) options </legend>
							<?= $this->Form->radio('view:compact:filter:sidebar',
								$SearchSettingOption['Filter']['Sidebar'], [
									'multiple' => true,
									'value' => $searchSettings['view:compact:filter:sidebar']
								]
							)?>
						</fieldset>

						<p class="ss-filter-head mt-4">
						Add/remove fields from search results
						<p>

						<div class="ml-3">
							<div class="mt-4">
								<p class="ss-filter-head">Object data</p>
								<fieldset>
									<legend class="d-none">Sidebar Object data list</legend>
									<?= $this->Form->select('view:compact:filter:object',
										$SearchSettingOption['Filter']['Object'], [
											'multiple' => 'checkbox',
											'value' => $searchSettings['view:compact:filter:object']
										]
									); ?>
								</fieldset>
							</div>

							<div class="mt-4">
								<p class="ss-filter-head">Textual data</p>
								<fieldset>
									<legend class="d-none">Sidebar Textual data list</legend>
									<?= $this->Form->select('view:compact:filter:textual', 
										$SearchSettingOption['Filter']['Textual'], [
											'multiple' => 'checkbox',
											'value' => $searchSettings['view:compact:filter:textual']
										]
									); ?>
								</fieldset>
							</div>

							<div class="mt-4">
								<p class="ss-filter-head">Publication data</p>
								<fieldset>
									<legend class="d-none">Sidebar Publication data list</legend>
									<?= $this->Form->select('view:compact:filter:publication',
										$SearchSettingOption['Filter']['Publication'], [
											'multiple' => 'checkbox',
											'value' => $searchSettings['view:compact:filter:publication']
										]
									); ?>
								</fieldset>
							</div>
						</div>
					</div>
						
				</div>
			</div>
			

			<div class="darken h-fit-content">
				<p class="form-title mb-0">Full view options</p>
				<span class="subscript-txt">Add/remove elements in full view</span>

				<div>
					<p class="ss-filter-head mt-4">
					Show/hide filter sidebar (desktop)
					<p>

					<fieldset>
						<legend class="d-none">Sidebar visibility(fullview) options </legend>
						<?= $this->Form->radio('view:full:filter:sidebar',
							$SearchSettingOption['Filter']['Sidebar'], [
								'multiple' => true,
								'value' => $searchSettings['view:full:filter:sidebar']
							]
						); ?>
					</fieldset>

					<p class="ss-filter-head mt-4">
					Add/remove fields from search results
					<p>

					<div class="ml-3">
						<div class="mt-4">
							<p class="ss-filter-head">Object data</p>
							<fieldset>
								<legend class="d-none">Fullview Object data list</legend>
								<?= $this->Form->select('view:full:filter:object',
									$SearchSettingOption['Filter']['Object'], [
										'multiple' => 'checkbox',
										'value' => $searchSettings['view:full:filter:object']
									]
								); ?>
							</fieldset>
						</div>

						<div class="mt-4">
							<p class="ss-filter-head">Textual data</p>
							<fieldset>
								<legend class="d-none">Fullview Textual data list</legend>
								<?= $this->Form->select('view:full:filter:textual',
									$SearchSettingOption['Filter']['Textual'], [
										'multiple' => 'checkbox',
										'value' => $searchSettings['view:full:filter:textual']
									]
								); ?>
							</fieldset>
						</div>

						<div class="mt-4">
							<p class="ss-filter-head">Publication data</p>
							<fieldset>
								<legend class="d-none">Fullview Publication data list</legend>
								<?= $this->Form->select('view:full:filter:publication',
									$SearchSettingOption['Filter']['Publication'], [
										'multiple' => 'checkbox',
										'value' => $searchSettings['view:full:filter:publication']
									]
								); ?>
							</fieldset>
						</div>
					</div>
				</div>

			</div>
		</div>
		
		<?= $this->Form->button('Save', [
			'type' => 'submit',
			'class' => 'wide-btn btn cdli-btn-blue mt-5'
			]
		); ?>
		<?= $this->Form->button('Reset', [
			'name' => 'Reset',
			'type' => 'submit',
			'class' => 'wide-btn btn cdli-btn mt-5'
			]
		); ?>
	<?= $this->Form->end()?>
</div>
<hr class="line"/>
<?= $this->Scroll->toTop() ?>