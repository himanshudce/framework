<article class="text-left">
    <h1>API Documentation</h1>

    <p>
        Most APIs work through <?= $this->Html->link(
            'Content Negotiation',
            'https://developer.mozilla.org/en-US/docs/Web/HTTP/Content_negotiation'
        ) ?>. Every API type lists the MIME types used for this. In some cases,
        regular file extensions are listed. They can be used as an alternative.
    </p>

    <nav>
        <h2>Contents</h2>
        <ul>
            <li><a href="#regular-data">Metadata</a></li>
            <li><a href="#linked-data">Linked data</a></li>
            <li><a href="#bibliographic-data">Bibliographies</a></li>
            <li><a href="#inscription-data">Inscriptions</a></li>
            <li><a href="#tabular-data">Tabular exports</a></li>
        </ul>
    </nav>

    <section class="border-bottom mt-3">
        <h2 id="regular-data">Metadata</h2>

        <p>
            Regular metadata export is enabled for almost everything. The schema
            is described <?= $this->Html->link(
                'here',
                '/docs/schema/1.0'
            ) ?>. The following formats are supported:
        </p>

        <table class="table">
            <thead>
                <?= $this->Html->tableHeaders(['Format', 'MIME type', 'File extension']) ?>
            </thead>
            <tbody>
                <?= $this->Html->tableCells([
                    ['JSON', 'application/json', '.json'],
                ]) ?>
            </tbody>
        </table>

        <hr>

        <p>The following paths are supported:</p>

        <?php
            $linkedData = [
                'abbreviations',
                'agadeMailsCdliTags',
                'agadeMails',
                'archives',
                'articlesAuthors',
                'articlesPublications',
                'articles',
                'artifactsCollections',
                'artifactsComposites',
                'artifactsDates',
                'artifactsExternalResources',
                'artifactsGenres',
                'artifactsLanguages',
                'artifactsMaterials',
                'artifactsPublications',
                'artifactsSeals',
                'artifactsShadow',
                'artifacts',
                'artifactsUpdates',
                'artifactTypes',
                'authorsPublications',
                'authors',
                'authorsUpdateEvents',
                'cdliTags',
                'collections',
                'dates',
                'dynasties',
                'editorsPublications',
                'editors',
                'entryTypes',
                'externalResources',
                'genres',
                'inscriptions',
                'journals',
                'languages',
                'materialAspects',
                'materialColors',
                'materials',
                'months',
                'periods',
                'postings',
                'postingTypes',
                'proveniences',
                'publications',
                'regions',
                'retiredArtifacts',
                'roles',
                'rolesUsers',
                'rulers',
                'signReadingsComments',
                'signReadings',
                'staff',
                'staffTypes',
                'updateEvents',
                'users',
                'years'
            ];
        ?>

        <ul>
            <?php foreach ($linkedData as $path): ?>
                <li>
                    <span class="badge badge-primary">GET</span>
                    <code>/<?= $path ?>/*</code>
                </li>
            <?php endforeach; ?>
        </ul>
    </section>

    <section class="border-bottom mt-3">
        <h2 id="linked-data">Linked data</h2>

        <p>
            The CDLI catalog supports Linked Open Data output for catalog data.
            The following formats are supported:
        </p>

        <table class="table">
            <thead>
                <?= $this->Html->tableHeaders(['Format', 'MIME type', 'File extension']) ?>
            </thead>
            <tbody>
                <?= $this->Html->tableCells([
                    ['JSON-LD', 'application/ld+json', '.jsonld'],
                    ['RDF/JSON', 'application/rdf+json', '-'],
                    ['RDF', 'application/rdf+xml', '.xml, .rdf'],
                    ['N-Triples', 'application/n-triples', '.nt'],
                    ['Turtle', 'text/turtle, application/turtle, application/x-turtle', '.ttl'],
                ]) ?>
            </tbody>
        </table>

        <hr>

        <p>The following paths, part of the main catalog, support linked data APIs:</p>

        <?php
            $linkedData = [
                // 'abbreviations',
                'archives',
                'artifacts',
                'artifactsExternalResources',
                'artifactsMaterials',
                // 'author',
                'collections',
                'dates',
                'dynasties',
                'genres',
                'inscriptions',
                // 'journals',
                'languages',
                'materials',
                'materialAspects',
                'materialColors',
                // 'months',
                'periods',
                'proveniences',
                'publications',
                'regions',
                'rulers',
                // 'signReadings',
                // 'signReadingsComment',
                // 'years'
            ];
        ?>

        <ul>
            <?php foreach ($linkedData as $path): ?>
                <li>
                    <span class="badge badge-primary">GET</span>
                    <code>/<?= $path ?>/*</code>
                </li>
            <?php endforeach; ?>
        </ul>
    </section>

    <section class="border-bottom mt-3">
        <h2 id="bibliographic-data">Bibliographies</h2>

        <p>
            Bibliography exports are supported for artifacts and publications.
            The following formats are supported:
        </p>

        <table class="table">
            <thead>
                <?= $this->Html->tableHeaders(['Format', 'MIME type', 'File extension']) ?>
            </thead>
            <tbody>
                <?= $this->Html->tableCells([
                    ['BibTeX', 'application/x-bibtex', '.bib'],
                    ['CSL-JSON', 'application/vnd.citationstyles.csl+json', '-'],
                    ['Formatted bibliographies', 'text/x-bibliography', '-'],
                    ['RIS', 'application/x-research-info-systems', '.ris'],
                ]) ?>
            </tbody>
        </table>

        <hr>

        <p>
            To specify the bibliographic style of the formatted bibliographies,
            use the <code>style</code> query parameter. Formatting is done using
            <?= $this->Html->link(
                'citeproc-js',
                'http://citeproc-js.readthedocs.io/'
            ) ?> and <?= $this->Html->link(
                'CSL styles',
                'https://citationstyles.org/'
            ) ?>. APA 7th (<code>apa</code>), Vancouver (<code>vancouver</code>)
            and Harvard Reference format 1 (author-date; <code>harvard1</code>)
            are supported.
        </p>

        <p>The following paths are supported:</p>

        <?php
            $linkedData = [
                'artifacts',
                'publications',
            ];
        ?>

        <ul>
            <?php foreach ($linkedData as $path): ?>
                <li>
                    <span class="badge badge-primary">GET</span>
                    <code>/<?= $path ?>/*</code>
                </li>
            <?php endforeach; ?>
        </ul>
    </section>

    <section class="border-bottom mt-3">
        <h2 id="inscription-data">Inscriptions</h2>

        <p>
            Inscriptions can be fetched from specific inscription versions or from
            the latest inscription of a given artifact. The following formats
            are supported:
        </p>

        <table class="table">
            <thead>
                <?= $this->Html->tableHeaders(['Format', 'MIME type', 'File extension']) ?>
            </thead>
            <tbody>
                <?= $this->Html->tableCells([
                    ['C-ATF', 'text/x-c-atf', '-'],
                    ['CDLI-CoNLL', 'text/x-cdli-conll', '-']
                    ['CoNLL-U', 'text/x-cdli-conll', '-']
                    ['CoNLL-RDF (as turtle)', 'text/x-conll+turtle', '-']
                ]) ?>
            </tbody>
        </table>

        <hr>

        <p>The following paths are supported:</p>

        <?php
            $linkedData = [
                'artifacts',
                'inscriptions',
            ];
        ?>

        <ul>
            <?php foreach ($linkedData as $path): ?>
                <li>
                    <span class="badge badge-primary">GET</span>
                    <code>/<?= $path ?>/*</code>
                </li>
            <?php endforeach; ?>
        </ul>
    </section>

    <section class="border-bottom mt-3">
        <h2 id="tabular-data">Tabular exports</h2>

        <p>
            Tabular exports are supported for artifacts, publications, and the
            links between the two (artifactsPublications). The following formats
            are supported:
        </p>

        <table class="table">
            <thead>
                <?= $this->Html->tableHeaders(['Format', 'MIME type', 'File extension']) ?>
            </thead>
            <tbody>
                <?= $this->Html->tableCells([
                    ['CSV', 'text/csv', '.csv'],
                    ['TSV', 'text/tab-separated-values', '.tsv'],
                    ['Excel', 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet', '.xlsx'],
                ]) ?>
            </tbody>
        </table>

        <hr>

        <p>The following paths are supported:</p>

        <?php
            $linkedData = [
                'artifacts',
                'artifactsPublications',
                'publications',
            ];
        ?>

        <ul>
            <?php foreach ($linkedData as $path): ?>
                <li>
                    <span class="badge badge-primary">GET</span>
                    <code>/<?= $path ?>/*</code>
                </li>
            <?php endforeach; ?>
        </ul>
    </section>
</article>
