<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsShadow $artifactsShadow
 */
?>
<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <div class="capital-heading"><?= __('Artifacts Shadow') ?></div>

        <table class="table-bootstrap">
            <tbody>
                <tr>
                    <th scope="row"><?= __('Artifact') ?></th>
                    <td><?= $artifactsShadow->has('artifact') ? $this->Html->link($artifactsShadow->artifact->id, ['controller' => 'Artifacts', 'action' => 'view', $artifactsShadow->artifact->id]) : '' ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Collection Location') ?></th>
                    <td><?= h($artifactsShadow->collection_location) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Collection Comments') ?></th>
                    <td><?= h($artifactsShadow->collection_comments) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Id') ?></th>
                    <td><?= $this->Number->format($artifactsShadow->id) ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Cdli Comments') ?></th>
                    <td><?= $this->Text->autoParagraph(h($artifactsShadow->cdli_comments)); ?></td>
                </tr>
                <tr>
                    <th scope="row"><?= __('Acquisition History') ?></th>
                    <td><?= $this->Text->autoParagraph(h($artifactsShadow->acquisition_history)); ?></td>
                </tr>
            </tbody>
        </table>

    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <!-- <?= $this->Html->link(__('Edit Artifacts Shadow'), ['action' => 'edit', $artifactsShadow->id], ['class' => 'btn-action']) ?> -->
        <!-- <?= $this->Form->postLink(__('Delete Artifacts Shadow'), ['action' => 'delete', $artifactsShadow->id], ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsShadow->id), 'class' => 'btn-action']) ?> -->
        <?= $this->Html->link(__('List Artifacts Shadow'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Artifacts Shadow'), ['action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
        <?= $this->Html->link(__('List Artifacts'), ['controller' => 'Artifacts', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <!-- <?= $this->Html->link(__('New Artifact'), ['controller' => 'Artifacts', 'action' => 'add'], ['class' => 'btn-action']) ?> -->
        <br/>
    </div>

</div>



