<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Publication $publication
 */
?>

<?php if ($flag == ''): ?>
    <div class="row justify-content-md-center">

    <div class="col-lg boxed">
        <?= $this->Form->create($publication) ?>
            <legend class="capital-heading"><?= __('Add Publication') ?></legend>
            <table cellpadding="10" cellspacing="10">
                <tr>
                    <td>Bibtex Key:</td>
                    <td><?php echo $this->Form->control('bibtexkey', ['label' => '', 'type' => 'text']); ?></td>
                </tr>
                <tr>
                    <td>Title:</td>
                    <td><?php echo $this->Form->control('title', ['label' => '', 'type' => 'text', 'maxLength' => 255]);  ?></td>
                </tr>
                <tr>
                    <td>Year:</td>
                    <td><?php echo $this->Form->control('year', ['label' => '', 'type' => 'text', 'maxLength' => 20]); ?></td>
                </tr>
                <tr>
                    <td>Entry Type:</td>
                    <td><?php echo $this->Form->control('entry_type_id', ['label' => '', 'options' => $entryTypes, 'empty' => true, 'class' => 'form-select']);  ?></td>
                </tr>
                <tr>
                    <td>Address:</td>
                    <td><?php echo $this->Form->control('address', ['label' => '', 'type' => 'text', 'maxLength' => 45]);  ?></td>
                </tr>
                <tr>
                    <td>Annote:</td>
                    <td><?php echo $this->Form->control('annote', ['label' => '', 'type' => 'text', 'maxLength' => 45]);  ?></td>
                </tr>
                <tr>
                    <td>Book Title:</td>
                    <td><?php echo $this->Form->control('book_title', ['label' => '', 'type' => 'text', 'maxLength' => 255]);  ?></td>
                </tr>
                <tr>
                    <td>Chapter:</td>
                    <td><?php echo $this->Form->control('chapter', ['label' => '', 'type' => 'text', 'maxLength' => 100]);  ?></td>
                </tr>
                <tr>
                    <td>Cross Reference:</td>
                    <td><?php echo $this->Form->control('crossref', ['label' => '', 'type' => 'text', 'maxLength' => 45]);  ?></td>
                </tr>
                <tr>
                    <td>Edition:</td>
                    <td><?php echo $this->Form->control('edition', ['label' => '', 'type' => 'text', 'maxLength' => 45]);  ?></td>
                </tr>
                <tr>
                    <td>How Published:</td>
                    <td><?php echo $this->Form->control('how_published', ['label' => '', 'type' => 'text', 'maxLength' => 255]);  ?></td>
                </tr>
                <tr>
                    <td>Institution:</td>
                    <td><?php echo $this->Form->control('institution', ['label' => '', 'type' => 'text', 'maxLength' => 45]);  ?></td>
                </tr>
                <tr>
                    <td>Journal:</td>
                    <td><?php echo $this->Form->control('journal_id', ['label' => '', 'options' => $journals, 'empty' => true, 'class' => 'form-select']);  ?></td>
                </tr>
                <tr>
                    <td>Month:</td>
                    <td><?php echo $this->Form->control('month', ['label' => '', 'type' => 'text', 'maxLength' => 45]);  ?></td>
                </tr>
                <tr>
                    <td>Note:</td>
                    <td><?php echo $this->Form->control('note', ['label' => '', 'type' => 'text', 'maxLength' => 45]);  ?></td>
                </tr>
                <tr>
                    <td>Number:</td>
                    <td><?php echo $this->Form->control('number', ['label' => '', 'type' => 'text', 'maxLength' => 100]);  ?></td>
                </tr>
                <tr>
                    <td>Organization</td>
                    <td><?php echo $this->Form->control('organization', ['label' => '', 'type' => 'text', 'maxLength' => 45]);  ?></td>
                </tr>
                <tr>
                    <td>Pages:</td>
                    <td><?php echo $this->Form->control('pages', ['label' => '', 'type' => 'text', 'maxLength' => 45]);  ?></td>
                </tr>
                <tr>
                    <td>Publisher:</td>
                    <td><?php echo $this->Form->control('publisher', ['label' => '', 'type' => 'text', 'maxLength' => 100]);  ?></td>
                </tr>
                <tr>
                    <td>School:</td>
                    <td><?php echo $this->Form->control('school', ['label' => '', 'type' => 'text', 'maxLength' => 80]);  ?></td>
                </tr>
                <tr>
                    <td>Volume:</td>
                    <td><?php echo $this->Form->control('volume', ['label' => '', 'type' => 'text', 'maxLength' => 50]);  ?></td>
                </tr>
                <tr>
                    <td>Publication History:</td>
                    <td><?php echo $this->Form->control('publication_history', ['label' => '', 'type' => 'text']);  ?></td>
                </tr>
                <tr>
                    <td>Series:</td>
                    <td><?php echo $this->Form->control('series', ['label' => '', 'type' => 'text', 'maxLength' => 100]);  ?></td>
                </tr>
                <tr>
                    <td>OCLC:</td>
                    <td><?php echo $this->Form->control('oclc', ['label' => '', 'type' => 'number']);  ?></td>
                </tr>
                <tr>
                    <td>Designation:</td>
                    <td><?php echo $this->Form->control('designation', ['label' => '', 'type' => 'text']); ?></td>
                </tr>
                <tr>
                    <td valign="top">Author List:</td>
                    <td valign="top">
                        <div class="form-group">
                            <input id="authors_input" name="authors" type="text" class="form-control">
                            <div id="input-foot-authors-parent" class="input-foot-authors-parent"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td valign="top">Editor List:</td>
                    <td valign="top">
                        <div class="form-group">
                            <input id="editors_input" name="editors" type="text" class="form-control">
                            <div id="input-foot-editors-parent" class="input-foot-editors-parent"></div>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td><?= $this->Form->submit('Submit', ['class' => 'btn btn-primary']) ?><td>
                </tr>
            </table>
        <?= $this->Form->end() ?>
    </div>

<?php elseif ($flag == 'bulk'): ?>

    <?= $this->cell('BulkUpload::confirmation', [
                        isset($error_list) ? $error_list:null, 
                        isset($header) ? $header:null, 
                        isset($entities) ? $entities:null]); ?>

    <?= $this->cell('BulkUpload', [
                            'Publications', 
                            isset($error_list) ? $error_list:null, 
                            isset($header) ? $header:null, 
                            isset($entities) ? $entities:null]); ?>
                            
<?php endif; ?>

<script src="/assets/js/autocomplete.js"></script>
