<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Journal $journal
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <form>
            <legend class="capital-heading"><?= __('Add article') ?></legend>
            <div class="form-group">
                <label>Select journal</label>
                <select id="add-journal-typeselect" class="form-control">
                <!-- <option>CDLJ</option> -->
                <option>CDLP</option>
                <!-- <option>CDLB</option> -->
                <option>CDLN</option>
                </select>
                <button onclick="OnTypeSelectSubmit()" type="button" class="btn btn-primary">Add</button>
            </div>
        </form>
    </div>
    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('List Journals'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>

<!-- Page script for admin/journals/add -->
<script>
function OnTypeSelectSubmit() {
    var type = $('#add-journal-typeselect').val();
    window.location.href='/admin/articles/add/' + type;
}
</script>