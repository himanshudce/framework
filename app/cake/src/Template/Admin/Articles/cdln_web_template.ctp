<html>
<head>
<title><?php echo $cdln[0]['title']?></title>
<script type="text/x-mathjax-config">
  MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
</script>
<script type="text/javascript" async
  src="/assets/js/mathjax/MathJax.js?config=TeX-AMS_HTML">
</script>
</head>
<style>

.cuneiform {
  font-family: CuneiformComposite;
  font-size: 12pt;
}

.hi {
  padding-left: 22px;
  text-indent: -22px;
}
.i {
  padding-left: 22px;
}
a {
  text-decoration: none;
  color: white;
}
p {
  margin-top: 0in;
  margin-right: 5pt;
  margin-bottom: 0in;
  margin-left: 5pt;
  margin-bottom: 0.0001pt;
  vertical-align: middle;
  font-family: Georgia, 'Times New Roman', Times, serif;
}
table {
  border-collapse: collapse;
  margin-top: 0in;
  margin-right: 5pt;
  margin-bottom: 0in;
  margin-left: 15pt;
  margin-bottom: 0.0001pt;
  font-family: Georgia, 'Times New Roman', Times, serif;
}
sub {
  vertical-align: -2pt;
  font-size: smaller;
}
sup {
  vertical-align: 2pt;
  font-size: smaller;
}
</style>
<body>
<table width="750" border="0" cellpadding="0" cellspacing="5">
	<tr>
	    <td valign="top" width="400"> 
	    <p style="font-family: S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif; font-size:9pt">
	        Cuneiform Digital Library Journal <br>
	        <b> 2018:001 </b> 
	        <br>
	        <font size="1"> ISSN 1540-8779 <br>
	        &#169; <i> Cuneiform Digital Library Initiative </i> </font> &nbsp; 
	    </p>
	    </td>
	    <td width="200" height="200" rowspan="2" align="right" nowrap bgcolor="#1461ab"> 
	    <p style="line-height:15.0pt; font-size:9pt;color:white;">
	        <font face="S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif"> <a href="/"> CDLI Home </a> <br>
	        <a href=""> CDLI Publications </a> <br>
	        <a href="" target="link"> Editorial Notes </a> <br>
	        <a href="" target="link"> Abbreviations </a> <br>
	        <a href="" target="link"> Bibliography </a> <br>
	        <br>
	        <a href="" target="blank">PDF Version of this Article </a> 
	        <br>
	        <a href="" target="link"> Get Acrobat Reader </a> <br>
	        <a href="" target="link"> <font color="#800517"><b>Download Cuneiform Font</b></font> </a> </font> 
	    </p>
	    </td>
	    <td width="1" rowspan="2" align="right" nowrap bgcolor="#99CCCC"> &nbsp; </td>
	</tr>
	<tr>
	    <td> 
	    <p>
	        <font face="S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif"> 
	        <h2 style="font-family: S&lt;sup&gt;ki&lt;/sup&gt;a, Verdana, Arial, Helvetica, sans-serif">
	        <br>
	        <p id="pArticleName"><?php echo $cdln[0]['title']; ?></p>
	        </h2>
	        <b> <p id="pArticleAuthors"> <?php echo $cdln[0]['authors']; ?> </p</b> <br>
	        <br>
	        <i>University</i> 
	        <br>
	        <br>
	        <b> Keywords </b> <br>
	        </font> 
	    </p>
	    </td>
	</tr>
	<tr>
	    <td colspan="3" align="center"> 
	    <hr align="center" width="600" size="2">
	    </td>
	</tr>
	<tr>
	    <td colspan="3" align="left"> 
	    <br>
	    <p>
	    <i><b>Abstract</b><br><br>
	    </p>
	    <div id="pArticleContent">
			<?php echo $cdln[0]['content_html']?>
		</div>
	    
	    </td>
	</tr>
	<tr>
		<td colspan="3" align="center"> 
		<hr>
		<p>
			<font face="S<sup>ki</sup>a, Verdana, Arial, Helvetica, sans-serif"> <b> Version: 
<!-- InstanceBeginEditable name="version date"-->
			<font face="S<sup>ki</sup>a, Verdana, Arial, Helvetica, sans-serif"> <b><?php echo $cdln[0]['modified']?></b> &nbsp; </font> 
<!-- InstanceEndEditable-->
			</b> </font> 
		</p>
		</td>
	</tr>
</table>
</body>
</html>
