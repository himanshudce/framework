<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\Author $author
 */
?>

<div class="row justify-content-md-center">

    <div class="col-lg-7 boxed">
        <?= $this->Form->create($author) ?>
            <legend class="capital-heading"><?= __('Add Author') ?></legend>
            <table cellpadding="10" cellspacing="10">
                <tr>
                    <td> First Name: </td>
                    <td><?php echo $this->Form->control('first', ['label' => '', 'type' => 'text', 'maxLength' => 149]) ?></td>
                </tr>
                <tr>
                    <td> Last Name: </td>
                    <td><?php echo $this->Form->control('last', ['label' => '', 'type' => 'text', 'maxLength' => 149]) ?></td>
                </tr>
                <tr>
                    <td> East Asian Order: </td>
                    <td><?php echo $this->Form->control('east_asian_order', ['label' => '', 'type' => 'checkbox']) ?></td>
                </tr>
                <tr>
                    <td> Email: </td>
                    <td><?php echo $this->Form->control('email', ['label' => '', 'type' => 'text', 'maxLength' => 150]) ?></td>
                </tr>
                <tr>
                    <td> Institution: </td>
                    <td><?php echo $this->Form->control('institution', ['label' => '', 'type' => 'text', 'maxLength' => 255]) ?></td>
                </tr>
                <tr>
                    <td> ORCID ID: </td>
                    <td><?php echo $this->Form->control('orcid_id', ['label' => '', 'type' => 'number', 'maxLength' => 16]) ?></td>
                </tr>
                <tr>
                    <td colspan="2">
                        <?= $this->Form->submit('Submit', ['class' => 'btn btn-primary']) ?>
                    <td>
                <tr>
            </table>
        <?= $this->Form->end() ?>
    </div>

    <div class="col-lg boxed">
        <div class="capital-heading"><?= __('Related Actions') ?></div>
        <?= $this->Html->link(__('Manage Author-Publication Links'), ['controller' => 'ArtifactsPublications', 'action' => 'add', 2, $author->id], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('Manage Editor-Publication Links'), ['controller' => 'ArtifactsPublications', 'action' => 'add', 2, $author->id], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Authors'), ['action' => 'index'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Cdl Notes'), ['controller' => 'CdlNotes', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Cdl Note'), ['controller' => 'CdlNotes', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Credits'), ['controller' => 'Credits', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Credit'), ['controller' => 'Credits', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Users'), ['controller' => 'Users', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New User'), ['controller' => 'Users', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
        <?= $this->Html->link(__('List Publications'), ['controller' => 'Publications', 'action' => 'index'], ['class' => 'btn-action']) ?>
        <?= $this->Html->link(__('New Publication'), ['controller' => 'Publications', 'action' => 'add'], ['class' => 'btn-action']) ?>
        <br/>
    </div>

</div>
