<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsPublication $artifactsPublication
 */
?>
<h1 class="display-3 header-text text-left"><?= __('Edit Artifact-Publication Link') ?></h1>

<?= $this->cell('PublicationView', [$artifactsPublication->publication->id]) ?> 
<div class="boxed mx-0">
        <legend class="capital-heading"><?= __('Artifact Information:') ?></legend>
        <?= $this->cell('ArtifactView', [$artifactsPublication->artifact_id]); ?>
</div>


<div class="row justify-content-md-center">

    <div class="col-lg boxed">
        <legend class="capital-heading"><?= __('Edit link for Artifact P'.substr("00000{$artifactsPublication->artifact_id}", -6).' and Publication') ?></legend>
        <?= $this->Form->create($artifactsPublication, ['action' => 'edit/'.$artifactsPublication->id.'/'.$flag.'/'.$parent_id]) ?>
        <table cellpadding="10" cellspacing="10">
            <?php echo $this->Form->control('publication_id', ['type' => 'hidden', 'value' => $artifactsPublication->publication->bibtexkey]); ?>
            <?php echo $this->Form->control('artifact_id', ['type' => 'hidden']); ?>
            <tr>
                <td> Exact Reference:  <td>
                <td><?php echo $this->Form->control('exact_reference', ['label' => '', 'type' => 'text', 'required' => false]); ?><td>
            </tr>
            <tr>
                <td> Publication Type: <td>
                <td><?php $options = [
                    'primary' => 'primary',
                    'electronic' => 'electronic',
                    'citation' => 'citation',
                    'collation' => 'collation',
                    'history' => 'history',
                    'other' => 'other'
                ];
                echo $this->Form->control('publication_type', ['label' => '', 'type' => 'select', 'options' => $options, 'value' => $artifactsPublication->publication_type, 'class' => 'form-select']); ?><td>
            </tr>
            <tr>
                <td> Publication Comments: <td>
                <td><?php echo $this->Form->control('publication_comments', ['label' => '', 'type' => 'textarea']); ?><td>
            </tr>
            <tr>
                <td><?= $this->Form->submit('Submit',['class' => 'btn cdli-btn-blue']) ?></td>
            </tr>
        </table>
        <?= $this->Form->end() ?>
    </div>
</div>
