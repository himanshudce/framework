<?php
/**
 * @var \App\View\AppView $this
 * @var \App\Model\Entity\ArtifactsPublication $artifactsPublication
 */
?>
<h1 class="display-3 header-text text-left"><?= __('Add Artifact-Publication Link') ?></h1>

<?php if ($flag == ''): ?>
    <div class="row justify-content-md-center">

        <div class="col-lg boxed">
            <?= $this->Form->create($artifactsPublication) ?>
                <legend class="capital-heading"><?= __('Link Artifact and Publication') ?></legend>
                <table cellpadding="10" cellspacing="10">
                    <tr>
                        <td> Artifact ID: <td>
                        <td><?= $this->Form->control('artifact_id', ['label' => '', 'type' => 'text']); ?></td>
                    </tr>
                    <tr>
                        <td> Publication BibTex Key: <td>
                        <td><?=  $this->Form->control('publication_id', ['label' => '', 'type' => 'text', 'maxLength' => 200, 'id' => 'bibtexkeyAutocomplete', 'list' => 'bibtexkeyList', 'autocomplete' => 'off']); ?>
                        <datalist id="bibtexkeyList"></datalist></td>
                    </tr>
                    <tr>
                        <td> Exact Reference: <td>
                        <td><?= $this->Form->control('exact_reference', ['label' => '', 'type' => 'text', 'maxLength' => 20, 'required' => false]); ?></td>
                    </tr>
                    <tr>
                        <td> Publication Type: <td>
                        <td><?php   $options = [
                                        'primary' => 'primary',
                                        'electronic' => 'electronic',
                                        'citation' => 'citation',
                                        'collation' => 'collation',
                                        'history' => 'history',
                                        'other' => 'other'
                                    ];
                                    echo $this->Form->control('publication_type', ['label' => '', 'type' => 'select', 'options' => $options, 'class' => 'form-select'] );?></td>
                    </tr>
                    <tr>
                        <td> Publication Comments: <td>
                        <td><?= $this->Form->control('publication_comments', ['label' => '', 'type' => 'textarea']); ?></td>
                    </tr>
                    <tr>
                        <td><?= $this->Form->submit('Submit',['class' => 'btn cdli-btn-blue']) ?></td>
                    </tr>
                </table>
            <?= $this->Form->end() ?>
        </div>
    </div>

<?php elseif ($flag == 'artifact'): ?>
    <?= $this->cell('PublicationView', [$id]) ?>

    <div class="row justify-content-md-center">
        <div class="col-lg boxed">
            <?= $this->Form->create($artifactsPublication, ['action' => 'add/'.'artifact'.'/'.$id]) ?>
                <legend class="capital-heading"><?= __('Link Artifact to this publication') ?></legend>
                <table cellpadding="10" cellspacing="10">
                    <?php  echo $this->Form->control('publication_id', ['type' => 'hidden', 'value' => $artifactsPublications->first()->publication->bibtexkey]); ?>
                    <tr>
                        <td> Artifact ID: <td>
                        <td><?= $this->Form->control('artifact_id', ['label' => '', 'type' => 'text']); ?></td>
                    </tr>
                    <tr>
                        <td> Exact Reference: <td>
                        <td><?= $this->Form->control('exact_reference', ['label' => '', 'type' => 'text', 'maxLength' => 20, 'required' => false]); ?></td>
                    </tr>
                    <tr>
                        <td> Publication Type: <td>
                        <td><?php   $options = [
                                        'primary' => 'primary',
                                        'electronic' => 'electronic',
                                        'citation' => 'citation',
                                        'collation' => 'collation',
                                        'history' => 'history',
                                        'other' => 'other'
                                    ];
                                    echo $this->Form->control('publication_type', ['label' => '', 'type' => 'select', 'options' => $options, 'class' => 'form-select'] );?></td>
                    </tr>
                    <tr>
                        <td> Publication Comments: <td>
                        <td><?= $this->Form->control('publication_comments', ['label' => '', 'type' => 'textarea'], ['length' => 10, 'width' => 10]); ?></td>
                    </tr>
                    <tr>
                        <td><?= $this->Form->submit('Submit',['class' => 'btn cdli-btn-blue']) ?></td>
                    </tr>
                </table>
            <?= $this->Form->end() ?>
        </div>
    </div>

    <h3 class="display-4 pt-3"><?= __('Linked Artifacts') ?></h3>

    <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
        <thead>
            <tr>
                <th scope="col">Artifact Identifier</th>
                <th scope="col">Artifact Designation</th>
                <th scope="col">Exact Reference</th>
                <th scope="col">Publication Type</th>
                <th scope="col">Publication Comments</th>
                <th scope="col"><?= __('Actions') ?></th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($artifactsPublications as $artifactsPublication): ?>
            <tr>
                <td><?= $artifactsPublication->has('artifact') ? $this->Html->link('P'.substr("00000{$artifactsPublication->artifact->id}", -6), "/artifacts/{$artifactsPublication->artifact->id}") : '' ?></td>
                <td><?= h($artifactsPublication->artifact->designation) ?></td>
                <td><?= h($artifactsPublication->exact_reference) ?></td>
                <td><?= h($artifactsPublication->publication_type) ?></td>
                <td><?= h($artifactsPublication->publication_comments) ?></td>
                <td>
                    <?= $this->Html->link(
                            $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                            ['action' => 'edit', $artifactsPublication->id, 'artifact', $id],
                            ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                    <?= $this->Form->postLink(
                            $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                            ['action' => 'delete', $artifactsPublication->id, 'artifact', $id],
                            ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsPublication->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div>
        <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
            <?= $this->Paginator->first() ?>
            <?= $this->Paginator->prev() ?>
            <?= $this->Paginator->numbers() ?>
            <?= $this->Paginator->next() ?>
            <?= $this->Paginator->last() ?>
        </ul>
        <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>

<?php elseif ($flag == 'publication'): ?>

    <div class="boxed mx-0">
        <legend class="capital-heading"><?= __('Artifact Information:') ?></legend>
        <?= $this->cell('ArtifactView', [$id]); ?>
    </div>
    <div class="row justify-content-md-center">

    <div class="col-lg boxed">
        <?= $this->Form->create($artifactsPublication, ['action' => 'add'.'/'.$flag.'/'.$id]) ?>
            <legend class="capital-heading"><?= __('Link Publication to the Artifact P'.substr("00000{$id}", -6)) ?></legend>
            <table cellpadding="10" cellspacing="10">
                    <?php echo $this->Form->control('artifact_id', ['type' => 'hidden', 'value' => $id]); ?>
                    <tr>
                        <td> Publication BibTex Key: <td>
                        <td><?=  $this->Form->control('publication_id', ['label' => '', 'type' => 'text', 'maxLength' => 200, 'id' => 'bibtexkeyAutocomplete', 'list' => 'bibtexkeyList', 'autocomplete' => 'off']); ?>
                        <datalist id="bibtexkeyList"></datalist></td>
                    </tr>

                    <tr>
                        <td> Exact Reference: <td>
                        <td><?= $this->Form->control('exact_reference', ['label' => '', 'type' => 'text', 'maxLength' => 20, 'required' => false]); ?></td>
                    </tr>
                    <tr>
                        <td> Publication Type: <td>
                        <td><?php   $options = [
                                        'primary' => 'primary',
                                        'electronic' => 'electronic',
                                        'citation' => 'citation',
                                        'collation' => 'collation',
                                        'history' => 'history',
                                        'other' => 'other'
                                    ];
                                    echo $this->Form->control('publication_type', ['label' => '', 'type' => 'select', 'options' => $options, 'class' => 'form-select'] );?></td>
                    </tr>
                    <tr>
                        <td> Publication Comments: <td>
                        <td><?= $this->Form->control('publication_comments', ['label' => '', 'type' => 'textarea']); ?></td>
                    </tr>
                    <tr>
                        <td><?= $this->Form->submit('Submit',['class' => 'btn cdli-btn-blue']) ?></td>
                    </tr>
            </table>
        <?= $this->Form->end() ?>
    </div>
    </div>

    <h3 class="display-4 pt-3"><?= __('Linked Publications') ?></h3>

    <table cellpadding="0" cellspacing="0" class="table-bootstrap my-3">
    <thead>
        <tr>
            <th scope="col">BibTex Key</th>
            <th scope="col">Publication Designation</th>
            <th scope="col">Publication Reference</th>
            <th scope="col">Publication Type</th>
            <th scope="col">Publication Comments</th>
            <th scope="col"><?= __('Actions') ?></th>
        </tr>
    </thead>
    <tbody>
        <?php foreach ($artifactsPublications as $artifactsPublication): ?>
        <tr>
            <td><?= $this->Html->link($artifactsPublication->publication->bibtexkey, "/publications/{$artifactsPublication->publication_id}") ?></td>
            <td><?= h($artifactsPublication->publication->designation) ?></td>
            <td><?= h($artifactsPublication->exact_reference) ?></td>
            <td><?= h($artifactsPublication->publication_type) ?></td>
            <td><?= h($artifactsPublication->publication_comments) ?></td>
            <td>
                <?= $this->Html->link(
                        $this->Html->tag('i', '', ['class' => 'fa fa-edit']),
                        ['action' => 'edit', $artifactsPublication->id, $flag, $id],
                        ['escape' => false, 'class' => 'btn btn-outline-success m-1', 'title' => 'Edit']) ?>
                <?= $this->Form->postLink(
                        $this->Html->tag('i', '', ['class' => 'fa fa-trash']),
                        ['action' => 'delete', $artifactsPublication->id, $flag, $id],
                        ['confirm' => __('Are you sure you want to delete # {0}?', $artifactsPublication->id), 'escape' => false, 'class' => 'btn btn-outline-danger m-1', 'title' => 'Delete']) ?>
            </td>
        </tr>
        <?php endforeach; ?>
    </tbody>
    </table>

    <div>
    <ul class="pagination pagination-dark my-4 d-flex justify-content-center">
        <?= $this->Paginator->first() ?>
        <?= $this->Paginator->prev() ?>
        <?= $this->Paginator->numbers() ?>
        <?= $this->Paginator->next() ?>
        <?= $this->Paginator->last() ?>
    </ul>
    <p><?= $this->Paginator->counter(['format' => __('Page {{page}} of {{pages}}, showing {{current}} record(s) out of {{count}} total')]) ?></p>
    </div>

<?php elseif ($flag == 'bulk'): ?>

<?= $this->cell('BulkUpload::confirmation', [
                        isset($error_list) ? $error_list:null, 
                        isset($header) ? $header:null, 
                        isset($entities) ? $entities:null]); ?>

    <div class="boxed mx-0">

<?= $this->cell('BulkUpload', [
                        'ArtifactsPublications', 
                        isset($error_list) ? $error_list:null, 
                        isset($header) ? $header:null, 
                        isset($entities) ? $entities:null]); ?>

<?php endif; ?>

<script src="/assets/js/jquery.min.js"></script>
<script src="/assets/js/autocomplete.js"></script>