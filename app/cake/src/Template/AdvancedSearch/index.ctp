

<div class="ads">
    <?= $this->Form->create("", ['type'=>'get']) ?>
    <div class="">
        <div class="d-flex align-items-center justify-content-between mb-3">
            <h1 class="display-3 text-left advanced-title">Advanced Search</h4>
            <?= $this->Form->button('Search', ['type' => 'submit', 'class' => 'search-btn btn d-none d-lg-block d-xl-block']); ?>
        </div>
        <div class="d-flex search-links">
            <?= $this->Html->link('Search settings', '#', ['class' => 'mr-5'])?>
            <?= $this->Html->link('Search aids', '#')?>
        </div>
    </div>

    <div class="text-left my-5">
        <label>Sort By:</label>
        <?php
            $options = ['R' => 'Relevance'];
            echo $this->Form->select('gender', $options, ['empty' => false, 'class' => 'ads-sort-select'])
        ?>
    </div>

    <div class="layout-grid text-left">
        <div>
            <div class="darken">
                <div class="d-flex align-items-baseline justify-content-between">
                    <p class="form-title">Publication Data</p>

                    <span class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" title="Tooltip on top"></span>
                </div>
                <?= $this->Form->control('Keyword:'); ?>
                <?= $this->Form->control('Designation:'); ?>
                <?= $this->Form->control('Author(s):'); ?>
                <?= $this->Form->control('Editor(s):'); ?>
                <?= $this->Form->control('Publication_year:'); ?>
                <?= $this->Form->control('Title:'); ?>
                <?= $this->Form->input('Publication_type:', array(
                    'options' => array("History","Primary","Electronic","Citation","Collation","Other"),
                    'empty' => 'Select a publication type'
                ));?>
                <?= $this->Form->control('Publisher:'); ?>
                <?= $this->Form->control('Series:'); ?>
                

            </div>

            <div class="darken ads-space-top">
                <div class="d-flex align-items-baseline justify-content-between">
                    <p class="form-title">Artifact Data</p>

                    <span class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" title="Tooltip on top"></span>
                </div>
                <?= $this->Form->control('Artifact_Type:'); ?>
                <?= $this->Form->control('Material:'); ?>
                <?= $this->Form->control('Collection:'); ?>
                <?= $this->Form->control('Provenience:'); ?>
                <?= $this->Form->control('Archive:'); ?>
                <?= $this->Form->control('Period:'); ?>
                <?= $this->Form->control('Artifact_Comments:'); ?>
            </div>
        </div>
        

        <div>
            <div class="darken ">
                <div class="d-flex align-items-baseline justify-content-between">
                    <p class="form-title">Inscriptions Data</p>

                    <span class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" title="Tooltip on top"></span>
                </div>
                <?= $this->Form->control('Translation:'); ?>
                <?= $this->Form->control('Transliteration:'); ?>
                <?= $this->Form->control('Comments:'); ?>
                <?= $this->Form->control('Structure:'); ?>

                <hr class="line"/>
                <?= $this->Form->control('Genre:'); ?>
                <?= $this->Form->control('Language:'); ?>
            </div>


            <div class="darken ads-space-top">
                <div class="d-flex align-items-baseline justify-content-between">
                    <p class="form-title">Artifact Identification</p>

                    <span class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" title="Tooltip on top"></span>
                </div>
                <?= $this->Form->control('Designation:'); ?>
                <?= $this->Form->control('Identification_Number:'); ?>
                <?= $this->Form->control('Museum Number:'); ?>
                <?= $this->Form->control('Accession Number:'); ?>
                <?= $this->Form->control('Artifact Number:'); ?>
                <?= $this->Form->control('Seal Number:'); ?>
                <?= $this->Form->control('Composite Number:'); ?>
            </div>

            <div class="darken ads-space-top ads-space-top">
                <div class="d-flex align-items-baseline justify-content-between">
                    <p class="form-title">Credits</p>

                    <span class="fa fa-question-circle" aria-hidden="true" data-toggle="tooltip" title="Tooltip on top"></span>
                </div>
                <?= $this->Form->control('Author/Contributor:'); ?>
                <?= $this->Form->control('Project:'); ?>
                <?= $this->Form->end()?>
            </div>
        </div>
    </div>
</div>
<?php if (!empty($result)) {?>
	<?php foreach ($result as $row) {?>

		<table>
			<tr>
				<th rowspan="10"> 
					<?php echo $row["id"];?>
				</th>
				<th > 
					<?php echo $row["_matchingData"]["Publications"]["designation"];?>
				</th>
			</tr>
			<tr>
				<td >
					<?php echo "Author: ";?>
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "Publications Date: ".$row["_matchingData"]["Publications"]["year"]; ?>
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "CDLI No.: ".$row["id"];?>
				</td>
			</tr>	
			<tr>
				<td >
					<?php echo "Collection: ".$row["_matchingData"]["Collections"]["collection"];?>
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "Provenience: ".$row["_matchingData"]["Proveniences"]["provenience"];?>
				</td>
			</tr>	
			<tr>
				<td >
					<?php echo "Period: ".$row["_matchingData"]["Periods"]["period"];?>
				</td>
			</tr>		
			<tr>
				<td >
					<?php echo "Object Type: ".$row["_matchingData"]["ArtifactTypes"]["artifact_type"];?>
				</td>
			</tr>	
			<tr>
				<td >
					<?php echo "Material: ".$row["_matchingData"]["Materials"]["material"];?>
				</td>
			</tr>	
			<tr>
				<td >
					<?php echo "Transliteration/Translation: ".$row["_matchingData"]["Inscriptions"]["transliteration"];?>
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "Languages: ".$row["_matchingData"]["Languages"]["language"];?>
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "Genres: ".$row["_matchingData"]["Genres"]["genre"];?>
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "ATF Source: ".$row["atf_source"];?>
					
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "Catalogue Source: ".$row["db_source"];?>
					
				</td>
			</tr>
			<tr>
				<td >
					<?php echo "Translation Source: ".$row["translation_source"];?>
					
				</td>
			</tr>
		</table>

	<?php }?>
<?php }?>

<?= $this->Form->button('Search', ['type' => 'submit', 'class' => 'wide-btn btn cdli-btn-blue mt-4']); ?>
<hr class="line" />
<?= $this->Scroll->toTop() ?>

<hr class="my-5">

<div class="visualize">
    <div class="pills-container d-flex justify-content-center">
        <ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
            <li class="nav-item">
                <a class="nav-link active" id="pills-bar-chart-tab" data-toggle="pill" href="#pills-bar-chart" role="tab" aria-controls="pills-bar-chart" aria-selected="true">Bar Chart</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="pills-donut-chart-tab" data-toggle="pill" href="#pills-donut-chart" role="tab" aria-controls="pills-donut-chart" aria-selected="false">Donut Chart</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="pills-radar-chart-tab" data-toggle="pill" href="#pills-radar-chart" role="tab" aria-controls="pills-radar-chart" aria-selected="false">Radar Chart</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="pills-line-chart-tab" data-toggle="pill" href="#pills-line-chart" role="tab" aria-controls="pills-line-chart" aria-selected="false">Line Chart</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="pills-dendrogram-chart-tab" data-toggle="pill" href="#pills-dendrogram-chart" role="tab" aria-controls="pills-dendrogram-chart" aria-selected="false">Dendrogram Chart</a>
            </li>

            <li class="nav-item">
                <a class="nav-link" id="pills-choropleth-map-tab" data-toggle="pill" href="#pills-choropleth-map" role="tab" aria-controls="pills-choropleth-map" aria-selected="false">Choropleth Map</a>
            </li>
        </ul>
    </div>
    
    <div class="tab-content" id="pills-tabContent">
        <div class="tab-pane fade show active" id="pills-bar-chart" role="tabpanel" aria-labelledby="pills-bar-chart-tab">
            <div id="bar-chart">
                <!--Viz Goes here-->
            </div>
            
            <!--Fallback Image-->
            <noscript>
                <?= $this->Html->image('d3-fallbacks/bar.png', ['class' => 'fallback-image', 'alt' => 'CDLI Bar Chart']) ?>
                <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
            </noscript>
        </div>
      
        <div class="tab-pane fade" id="pills-donut-chart" role="tabpanel" aria-labelledby="pills-donut-chart-tab">
            <div id="donut-chart">
                <!--Viz Goes here-->
            </div>
            
            <!--Fallback Image-->
            <noscript>
                <?= $this->Html->image('d3-fallbacks/donut.png', ['class' => 'fallback-image', 'alt' => 'CDLI Donut Chart']) ?>
                <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
            </noscript>
        </div>
        
        <div class="tab-pane fade" id="pills-radar-chart" role="tabpanel" aria-labelledby="pills-radar-chart-tab">
            <div id="radar-chart">
                <!--Viz Goes here-->
            </div>
            
            <!--Fallback Image-->
            <noscript>
                <?= $this->Html->image('d3-fallbacks/radar.png', ['class' => 'fallback-image', 'alt' => 'CDLI Radar Chart']) ?>
                <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
            </noscript>
        </div>
        
        <div class="tab-pane fade" id="pills-line-chart" role="tabpanel" aria-labelledby="pills-line-chart-tab">
            <div class="d-flex">
                <div class="ml-auto mb-3">
                    <form class="form-inline">
                        <div class="form-group">
                            <label class="mr-3" for="line-chart-filter">Line Chart </label>
                            <select class="form-control" id="line-chart-filter">
                                <option value="period-vs-genre" selected>Period vs Genre</option>
                                <option value="period-vs-language">Period vs Language</option>
                                <option value="period-vs-material">Period vs Material</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
            <div id="line-chart">
                <!--Viz Goes here-->
            </div>
            
            <!--Fallback Image-->
            <noscript>
                <?= $this->Html->image('d3-fallbacks/line.png', ['class' => 'fallback-image', 'alt' => 'CDLI Line Chart']) ?>
                <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
            </noscript>
        </div>
        
        <div class="tab-pane fade" id="pills-dendrogram-chart" role="tabpanel" aria-labelledby="pills-dendrogram-chart-tab">
            <div id="dendrogram-chart">
                <!--Viz Goes here-->
            </div>
            
            <!--Fallback Image-->
            <noscript>
                <?= $this->Html->image('d3-fallbacks/dendrogram.png', ['class' => 'fallback-image', 'alt' => 'CDLI Dendrogram Chart']) ?>
                <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
            </noscript>
        </div>
        
        <div class="tab-pane fade" id="pills-choropleth-map" role="tabpanel" aria-labelledby="pills-choropleth-map-tab">
            <div class="d-flex">
                <div class="map-controls-container">
                    <div class="btn-group" role="group" aria-label="Map Controls">
                        <button type="button" id="zoom-in" class="btn btn-secondary">
                            Zoom In <span class="fa fa-search-plus ml-2" aria-hidden="true"></span>
                        </button>
                        <button type="button" id="zoom-out" class="btn btn-secondary">
                            Zoom Out <span class="fa fa-search-minus ml-2" aria-hidden="true"></span>
                        </button>
                        <button type="button" id="reset" class="btn btn-secondary">
                            Reset
                        </button>
                    </div>
                </div>
                
                <div class="ml-auto mb-3">
                    <form class="form-inline">
                        <div class="form-group">
                            <label class="mr-3" for="choropleth-map-filter">Filter Map By</label>
                            <select class="form-control" id="choropleth-map-filter">
                                <option value="proveniences" selected>Proveniences</option>
                                <option value="regions">Regions</option>
                            </select>
                        </div>
                    </form>
                </div>
            </div>
            <div id="choropleth-map">
                <!--Viz Goes here-->
            </div>
            
            <!--Fallback Image-->
            <noscript>
                <?= $this->Html->image('d3-fallbacks/choropleth.png', ['class' => 'fallback-image', 'alt' => 'CDLI Choropleth Map']) ?>
                <p class="mt-5">Please enable JavaScript to interact with the visualization.</p>
            </noscript>
        </div>
    </div>
</div>

<?php echo $this->Html->script(['d3', 'd3-compiled']); ?>

<script type="text/javascript">
    var barChartData = <?php echo $barChart; ?>;
    var donutChartData = <?php echo $donutChart; ?>;
    var radarChartData = <?php echo $radarChart; ?>;
    var lineChartData = <?php echo $lineChart; ?>;
    var dendrogramChartData = <?php echo $dendrogramChart; ?>;
    var choroplethMapData = <?php echo $choroplethMap; ?>;
    
    barChart(barChartData);
    donutChart(donutChartData);
    radarChart(radarChartData);
    lineChart(lineChartData);
    dendrogramChart(dendrogramChartData);
    choroplethMap(choroplethMapData[0], "proveniences");
    
    
    // Line Chart Select Filter
    $('#line-chart-filter').change(function() {        
        var lineChartType = $(this).val();
        var targetURL = "<?= \Cake\Routing\Router::url(array('controller' => 'AdvancedSearch', 'action' => 'getUpdatedLineChartData'), true) ?>";
        var csrfToken = <?= json_encode($this->request->getParam('_csrfToken')) ?>;
        
        // Create AJAX request for retrieving updated data
        $.ajax({
            type: "POST",
            url: targetURL,
            headers: { 'X-CSRF-Token': csrfToken },
            data: {lineChartType: lineChartType},
            dataType: "JSON",
            success: function(response) {
                // Remove the current SVG
                $("#line-chart").empty();
                
                // Add the new line chart
                lineChart(response);
            }
        })
    })
    
    
    // Get choropleth map by filter
    // 0 : Proveniences
    // 1 : Regions
    $('#choropleth-map-filter').change(function() {        
        // Remove the current SVG
        $("#choropleth-map").empty();
        
        var choroplethMapType = $(this).val();
        if (choroplethMapType == "proveniences")
            choroplethMap(choroplethMapData[0], choroplethMapType);
        else
            choroplethMap(choroplethMapData[1], choroplethMapType);
    })
</script>
