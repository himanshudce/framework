<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\Http\Exception\UnauthorizedException;
use Cake\Http\Exception\NotAcceptableException;

/**
 * Inscriptions Controller
 *
 * @property \App\Model\Table\InscriptionsTable $Inscriptions
 *
 * @method \App\Model\Entity\Inscription[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class InscriptionsController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('GeneralFunctions');
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Inscription');
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|null
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['Artifacts'],
        ];
        $inscriptions = $this->paginate($this->Inscriptions);

        $this->set(compact('inscriptions'));
    }

    /**
     * View method
     *
     * @param string|null $id Inscription id.
     * @return \Cake\Http\Response|null
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $inscription = $this->Inscriptions->get($id, [
            'contain' => ['Artifacts']
        ]);

        if (!$inscription->artifact->is_atf_public &&
            !$this->GeneralFunctions->checkIfRolesExists([1, 2, 5])) {
            throw new UnauthorizedException();
        }

        $type = $this->Inscription->getPreferredType($inscription);
        if (empty($type)) {
            throw new NotAcceptableException();
        }
        $this->RequestHandler->renderAs($this, $type);

        $this->set('inscription', $inscription);
        $this->set('_serialize', 'inscription');
    }
}
