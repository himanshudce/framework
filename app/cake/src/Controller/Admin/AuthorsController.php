<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Authors Controller
 *
 * @property \App\Model\Table\AuthorsTable $Authors
 *
 * @method \App\Model\Entity\Author[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class AuthorsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->paginate = [
            'limit' => 10,
            'order' => [
                'first' => 'ASC',
                'last' => 'ASC'
            ]
        ];

        $authors = $this->paginate($this->Authors);

        $this->set(compact('authors'));
    }

    /**
     * View method
     *
     * @param string|null $id Author id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $author = $this->Authors->get($id, [
            // 'contain' => ['Publications', 'CdlNotes', 'Credits', 'Users']
        ]);

        $this->set(compact('author'));
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $author = $this->Authors->newEntity();
        if ($this->request->is('post')) {
            $data = $this->request->getData();
            $author = $this->Authors->patchEntity($author, $data);
            if ($data['last'].$data['first'] == '') {
                $author->setErrors([
                    'first' => ['Both first and last name cannot be empty'],
                    'last' => ['Both first and last name cannot be empty']
                    ]);
            } elseif ($this->Authors->save($author)) {
                $this->Flash->success(__('The author has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The author could not be saved. Please, try again.'));
        }
        $this->set(compact('author'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Author id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $author = $this->Authors->get($id);
        

        if ($this->request->is(['patch', 'post', 'put'])) {
            $data = $this->request->getData();
            $author = $this->Authors->patchEntity($author, $data);
            if ($data['last'].$data['first'] == '') {
                $author->setErrors([
                    'first' => ['Both first and last name cannot be empty'],
                    'last' => ['Both first and last name cannot be empty']
                    ]);
            } elseif ($this->Authors->save($author)) {
                $this->Flash->success(__('The author details have been updated.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The author details could not be updated. Please, try again.'));
        }
        $this->set(compact('author'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Author id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->request->allowMethod(['post', 'delete']);
        $author = $this->Authors->get($id);
        if ($this->Authors->delete($author)) {
            $this->Flash->success(__('The author has been deleted.'));
        } else {
            $this->Flash->error(__('The author could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    /**
     * authorSearchAjax method
     *
     * @return list of authors.
     */
    public function authorSearchAjax()
    {
        $this->autoRender = false;
        $search_key = $this->request->params["author"];

        $authors =  $this->Authors->find('all', [
            'fields' => ['author'],
            'order' => ['author' => 'asc']
            ])->where(['author LIKE' => '%'. $search_key.'%'])->all();
        $authors = json_encode($authors) ;
        
        echo $authors;
    }

    /**
     * addAuthorAjax method
     *
     * @return list of authors.
     */
    public function addAuthorAjax()
    {
        $this->autoRender = false;
        $author_name = $this->request->params["author"];

        $author = $this->Authors->newEntity();
        $author->author = $author_name;

        $this->Authors->save($author);

        echo $author;
    }
}
