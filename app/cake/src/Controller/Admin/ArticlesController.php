<?php
namespace App\Controller\Admin;

use App\Controller\AppController;
use Cake\Http\Exception\ForbiddenException;
use Cake\Datasource\ConnectionManager;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\JournalsTable $Journals
 *
 * @method \App\Model\Entity\Journal[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArticlesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Articles');
        $this->loadModel('ArticlesAuthors');
        $this->loadModel('Authors');
    }

    /**
    * authorToAuthorID helper.
    */
    public function authorToAuthorID($authorNames)
    {
        $authorIds = array();
        $authorNames = explode(',', $authorNames);
        foreach ($authorNames as $authorName) {
            $author =  $this->Authors->find()->where(['author' => $authorName])->first();

            if (!$author) {
                $author = $this->Authors->newEntity();
                $author->author = $authorName;
                $this->Authors->save($author);
            }
            array_push($authorIds, $author->id);
        }

        return $authorIds;
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        $this->set(compact(''));
    }

    /**
     * Add CDLN Artcile.
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function addCdln()
    {
        if ($this->request->is('post')) {
            $this->autoRender = false;
            $requestData = $this->request->getData();
            $article = $this->Articles->newEntity();
            $article->title = $requestData['cdln_title'];
            $article->content_html = $requestData['cdln_content'];
            $article->content_latex = '';
            $article->is_pdf_uloaded = 1;
            $article->article_type = 'cdln';
            $article->is_published = 0 ;
            $article->article_status = $requestData['cdln_status'];
            $article->pdf_link = '';
            $article->created_by = 0 ;
            $article->created = date('y/m/d');
            $article->modified =  date('y/m/d');

            $this->Articles->save($article);

            $authorIds = $this->authorToAuthorID($requestData['cdln_authors']);
            $sequence = 1;
            foreach ($authorIds as $authorId) {
                $article_autor = $this->ArticlesAuthors->newEntity();
                $article_autor->article_id = $article->id;
                $article_autor->author_id = $authorId;
                $article_autor->sequence =$sequence++;
                
                $this->ArticlesAuthors->save($article_autor);
            }

            return $this->redirect('/admin/articles/cdln');
        }
    }

    /**
     * Add CDLP Artcile.
     */
    public function addCdlp()
    {
        if ($this->request->is('post')) {
            $this->autoRender = false;
            $requestData = $this->request->getData();
    
            $article = $this->Articles->newEntity();
            $article->title = $requestData['cdlp_title'];
            $article->content_html = '';
            $article->content_latex = '';
            $article->is_pdf_uloaded = 1;
            $article->article_type = 'cdlp';
            $article->is_published = 0 ;
            $article->article_status = $requestData['cdlp_status'];
            $article->pdf_link = $requestData['cdlp_pdf_link'];
            $article->created_by = 0 ;
            $article->created = date('y/m/d');
            $article->modified =  date('y/m/d');

            $this->Articles->save($article);

            $authorIds = $this->authorToAuthorID($requestData['cdlp_authors']);
            $sequence = 1;
            foreach ($authorIds as $authorId) {
                $article_autor = $this->ArticlesAuthors->newEntity();
                $article_autor->article_id = $article->id;
                $article_autor->author_id = $authorId;
                $article_autor->sequence =$sequence++;
                
                $this->ArticlesAuthors->save($article_autor);
            }

            return $this->redirect('/admin/articles/cdlp');
        }
    }


    /**
     * Delete Artcile.
     *
     */
    public function deleteArticle()
    {
        $this->autoRender = false;
        if (1) {
            $article_id = $this->request->params["article"];

            $this->ArticlesAuthors->deleteAll(['article_id' => $article_id]);
            $this->Articles->deleteAll(['id' => $article_id]);
            echo 'deleted';
        } else {
            throw new ForbiddenException(__('Request not allowed.'));
        }
    }

    /**
     * Edit CDLN Artcile.
     *
     */
    public function editCdln()
    {
        $connection = ConnectionManager::get('default');
        $article_id = $this->request->params["article"];
        $article =  $this->Articles->find()->where(['id' => $article_id, 'article_type' => 'cdln'])->first();
        
        $authors = $connection->execute("select authors.author from articles_authors join authors where
         articles_authors.article_id = ".$article_id." and articles_authors.author_id = authors.id")->fetchAll('assoc');

        $authorNames = array();
        foreach ($authors as $author) {
            array_push($authorNames, $author['author']);
        }
        
        if ($article) {
            $this->set(['article' => $article, 'authorNames'=> implode($authorNames, ',')]);
        } else {
            throw new ForbiddenException(__('No such CDLN to edit.'));
        }

        if ($this->request->is('post')) {
            $requestData = $this->request->getData();
    
            $article->title = $requestData['cdln_title'];
            $article->content_html = $requestData['cdln_content'];
            $article->content_latex = '';
            $article->is_pdf_uloaded = 1;
            $article->article_type = 'cdln';
            $article->is_published = 0 ;
            $article->article_status = $requestData['cdln_status'];
            $article->pdf_link = $requestData['cdln_pdf_link'];
            $article->created_by = 0 ;
            $article->created = date('y/m/d');
            $article->modified =  date('y/m/d');

            $this->Articles->save($article);

            $this->ArticlesAuthors->deleteAll(['article_id' => $article->id]);

            $authorIds = $this->authorToAuthorID($requestData['cdln_authors']);
            $sequence = 1;
            foreach ($authorIds as $authorId) {
                $article_autor = $this->ArticlesAuthors->newEntity();
                $article_autor->article_id = $article->id;
                $article_autor->author_id = $authorId;
                $article_autor->sequence =$sequence++;
                
                $this->ArticlesAuthors->save($article_autor);
            }

            return $this->redirect('/admin/articles/cdln');
        }
    }

    /**
     * Edit CDLP Artcile.
     *
     */
    public function editCdlp()
    {
        $connection = ConnectionManager::get('default');
        $article_id = $this->request->params["article"];
        $article =  $this->Articles->find()->where(['id' => $article_id, 'article_type' => 'cdlp'])->first();
        
        $authors = $connection->execute("select authors.author from articles_authors join authors where
         articles_authors.article_id = ".$article_id." and articles_authors.author_id = authors.id")->fetchAll('assoc');

        $authorNames = array();
        foreach ($authors as $author) {
            array_push($authorNames, $author['author']);
        }
        
        if ($article) {
            $this->set(['article' => $article, 'authorNames'=> implode($authorNames, ',')]);
        } else {
            throw new ForbiddenException(__('No such CDLP to edit.'));
        }

        if ($this->request->is('post')) {
            $requestData = $this->request->getData();
    
            $article->title = $requestData['cdlp_title'];
            $article->content_html = '';
            $article->content_latex = '';
            $article->is_pdf_uloaded = 1;
            $article->article_type = 'cdlp';
            $article->is_published = 0 ;
            $article->article_status = $requestData['cdlp_status'];
            $article->pdf_link = $requestData['cdlp_pdf_link'];
            $article->created_by = 0 ;
            $article->created = date('y/m/d');
            $article->modified =  date('y/m/d');

            $this->Articles->save($article);

            $this->ArticlesAuthors->deleteAll(['article_id' => $article->id]);

            $authorIds = $this->authorToAuthorID($requestData['cdlp_authors']);
            $sequence = 1;

            foreach ($authorIds as $authorId) {
                $article_autor = $this->ArticlesAuthors->newEntity();
                $article_autor->article_id = $article->id;
                $article_autor->author_id = $authorId;
                $article_autor->sequence =$sequence++;
                
                $this->ArticlesAuthors->save($article_autor);
            }

            return $this->redirect('/admin/articles/cdlp');
        }
    }

    /**
     * Upload Article Helper.
     */
    public function uploadArticlePdf()
    {
        $this->autoRender = false;
        $article = $this->request->params["article"];
        $article = strtolower($article);
        if ($this->request->is('post')) {
            $file = $this->request->getData('file');
        
            if ($file['type']=='application/pdf') {
                $CDLNFilePath = 'webroot/pubs/'.$article.'/'.$file['name'];
                if (move_uploaded_file($file["tmp_name"], $CDLNFilePath)) {
                    echo $file['name'];
                } else {
                    echo 'File is not uploaded.';
                }
            } else {
                throw new ForbiddenException(__('Only pdf files are allowed.'));
            }
        }
    }

    /**
    * indexCdln method
    */
    public function indexCdln()
    {
        $connection = ConnectionManager::get('default');
        
        $articles = $connection->execute("select * , articles.id as article_id,GROUP_CONCAT(authors.author) as authors from articles join articles_authors, authors where articles.article_type='cdln'
        and articles.id = articles_authors.article_id and articles_authors.author_id = authors.id group by articles.id; ")->fetchAll('assoc');

        $this->set(['cdln' => $articles ]);
    }

    /**
     * indexCdlp method
     */
    public function indexCdlp()
    {
        $connection = ConnectionManager::get('default');
        
        $articles = $connection->execute("select * , articles.id as article_id,GROUP_CONCAT(authors.author) as authors from articles join articles_authors, authors where articles.article_type='cdlp'
        and articles.id = articles_authors.article_id and articles_authors.author_id = authors.id group by articles.id; ")->fetchAll('assoc');

        $this->set(['cdlp' => $articles ]);
    }
    
    /**
     * viewCdln method
     */
    public function viewCdln()
    {
        $this->autoRender = false;

        $article = $this->request->params["cdln"];
        $article =  $this->Articles->find()->where(['id' => $article, 'article_type' => 'cdln'])->first();

        if ($article) {
            $this->response->file(
                'webroot/pubs/cdln/'.$article->pdf_link
            );
        } else {
            throw new ForbiddenException(__('No such CDLN article.'));
        }
    }

    /**
     * viewCdln method
     */
    public function viewCdlnWeb()
    {
        $this->autoRender = false;
        
        $article = $this->request->params["cdln"];

        $connection = ConnectionManager::get('default');
        
        $article = $connection->execute("select * , articles.id as article_id, GROUP_CONCAT(authors.author) as authors from articles join articles_authors, authors where articles.id=".$article." and articles.article_type='cdln'
        and articles.id = articles_authors.article_id and articles_authors.author_id = authors.id group by articles.id; ")->fetchAll('assoc');


        if ($article) {
            $this->layout= '';
            $this->set(['cdln' => $article ]);
            $this->render('cdln_web_template');
        } else {
            throw new ForbiddenException(__('No such CDLN article.'));
        }
    }
    
    /**
     * viewCdlp method
     */
    public function viewCdlp()
    {
        $this->autoRender = false;
        $article = $this->request->params["cdlp"];
        $article =  $this->Articles->find()->where(['id' => $article, 'article_type' => 'cdlp'])->first();
        if ($article) {
            $this->response->file(
                'webroot/pubs/cdlp/'.$article->pdf_link
            );
        } else {
            throw new ForbiddenException(__('No such CDLP article.'));
        }
    }
}
