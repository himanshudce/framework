<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

/**
 * Artifacts Controller
 *
 * @property \App\Model\Table\ArtifactsTable $Artifacts
 *
 * @method \App\Model\Entity\Artifact[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArtifactsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->paginate = [
            'contain' => ['Proveniences', 'Periods', 'ArtifactTypes', 'Archives']
        ];
        $artifacts = $this->paginate($this->Artifacts);

        $this->set(compact('artifacts'));
    }

    /**
     * View method
     *
     * @param string|null $id Artifact id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $artifact = $this->Artifacts->get($id, [
            'contain' => [
                'Proveniences',
                'Periods',
                'ArtifactTypes',
                'Archives',
                // 'Credits',
                // 'Credits.Authors',
                'Collections',
                'Dates',
                'ExternalResources',
                'Genres',
                'Languages',
                'Materials',
                'Publications',
                'Publications.Authors',
                'Publications.EntryTypes',
                'Publications.Journals',
                // 'ArtifactDates',
                'ArtifactsComposites',
                // 'ArtifactsDateReferenced',
                'ArtifactsSeals',
                'ArtifactsShadow',
                'Inscriptions',
                'RetiredArtifacts'
            ]
        ]);

        $this->set('artifact', $artifact);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1, 2])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $artifact = $this->Artifacts->newEntity();
        if ($this->request->is('post')) {
            $artifact = $this->Artifacts->patchEntity($artifact, $this->request->getData());
            if ($this->Artifacts->save($artifact)) {
                $this->Flash->success(__('The artifact has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifact could not be saved. Please, try again.'));
        }
        $proveniences = $this->Artifacts->Proveniences->find('list', ['limit' => 200]);
        $periods = $this->Artifacts->Periods->find('list', ['limit' => 200]);
        $artifactTypes = $this->Artifacts->ArtifactTypes->find('list', ['limit' => 200]);
        $archives = $this->Artifacts->Archives->find('list', ['limit' => 200]);
        $collections = $this->Artifacts->Collections->find('list', ['limit' => 200]);
        $dates = $this->Artifacts->Dates->find('list', ['limit' => 200]);
        $externalResources = $this->Artifacts->ExternalResources->find('list', ['limit' => 200]);
        $genres = $this->Artifacts->Genres->find('list', ['limit' => 200]);
        $languages = $this->Artifacts->Languages->find('list', ['limit' => 200]);
        $materials = $this->Artifacts->Materials->find('list', ['limit' => 200]);
        $publications = $this->Artifacts->Publications->find('list', ['limit' => 200]);

        $this->set(compact(
            'artifact',
            'proveniences',
            'periods',
            'artifactTypes',
            'archives',
            'collections',
            'dates',
            'externalResources',
            'genres',
            'languages',
            'materials',
            'publications'
        ));
    }

    /**
     * Edit method
     *
     * @param string|null $id Artifact id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1, 2])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $artifact = $this->Artifacts->get($id, [
            'contain' => [
                'Collections',
                'Dates',
                'ExternalResources',
                'Genres',
                'Languages',
                'Materials',
                'Publications'
            ]
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $artifact = $this->Artifacts->patchEntity($artifact, $this->request->getData());
            if ($this->Artifacts->save($artifact)) {
                $this->Flash->success(__('The artifact has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The artifact could not be saved. Please, try again.'));
        }
        $proveniences = $this->Artifacts->Proveniences->find('list', ['limit' => 200]);
        $periods = $this->Artifacts->Periods->find('list', ['limit' => 200]);
        $artifactTypes = $this->Artifacts->ArtifactTypes->find('list', ['limit' => 200]);
        $archives = $this->Artifacts->Archives->find('list', ['limit' => 200]);
        $collections = $this->Artifacts->Collections->find('list', ['limit' => 200]);
        $dates = $this->Artifacts->Dates->find('list', ['limit' => 200]);
        $externalResources = $this->Artifacts->ExternalResources->find('list', ['limit' => 200]);
        $genres = $this->Artifacts->Genres->find('list', ['limit' => 200]);
        $languages = $this->Artifacts->Languages->find('list', ['limit' => 200]);
        $materials = $this->Artifacts->Materials->find('list', ['limit' => 200]);
        $publications = $this->Artifacts->Publications->find('list', ['limit' => 200]);

        $this->set(compact(
            'artifact',
            'proveniences',
            'periods',
            'artifactTypes',
            'archives',
            'collections',
            'dates',
            'externalResources',
            'genres',
            'languages',
            'materials',
            'publications'
        ));
    }

    /**
     * Delete method
     *
     * @param string|null $id Artifact id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->request->allowMethod(['post', 'delete']);
        $artifact = $this->Artifacts->get($id);
        if ($this->Artifacts->delete($artifact)) {
            $this->Flash->success(__('The artifact has been deleted.'));
        } else {
            $this->Flash->error(__('The artifact could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }
}
