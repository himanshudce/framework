<?php
namespace App\Controller\Admin;

use App\Controller\AppController;

use ArrayObject;
use Cake\ORM\TableRegistry;

/**
 * Publications Controller
 *
 * @property \App\Model\Table\PublicationsTable $Publications
 *
 * @method \App\Model\Entity\Publication[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PublicationsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Load Component 'GeneralFunctions'
        $this->loadComponent('GeneralFunctions');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->paginate = [
            'contain' => ['EntryTypes', 'Journals']
        ];
        $publications = $this->paginate($this->Publications);

        $this->set(compact('publications'));
    }

    /**
     * View method
     *
     * @param string|null $id Publication id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $publication = $this->Publications->get($id, [
            'contain' => ['EntryTypes', 'Journals', 'Abbreviations', 'Authors']
        ]);

        $this->set('publication', $publication);
    }

    /**
     * Add method
     *
     * @return \Cake\Http\Response|null Redirects on successful add, renders view otherwise.
     */
    public function add($flag = '')
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        if ($flag == '') {
            $publication = $this->Publications->newEntity();
            if ($this->request->is('post')) {
                $data = $this->request->getData();
                $publication = $this->Publications->patchEntity($publication, $data, ['associated' => ['Authors', 'Editors']]);
                if ($this->Publications->save($publication)) {
                    $this->Flash->success(__('The publication has been saved.'));
    
                    return $this->redirect(['action' => 'index']);
                }
                $this->Flash->error(__('The publication could not be saved. Please, try again.'));
            }
            $entryTypes = $this->Publications->EntryTypes->find('list', [
                'keyField' => 'id',
                'valueField' => 'label',
                'order' => 'label'
            ])->toArray();
            $journals = $this->Publications->Journals->find('list', [
                'keyField' => 'id',
                'valueField' => 'journal',
                'order' => 'journal'
            ])->toArray();
            $this->set(compact('publication', 'entryTypes', 'journals', 'flag'));
        } elseif ($flag == 'bulk') {
            $this->loadComponent('BulkUpload', ['table' => 'Publications']);
            $this->BulkUpload->upload();
            
            $this->set(compact('flag'));
        }
        $entryTypes = $this->Publications->EntryTypes->find('list', [
            'keyField' => 'id',
            'valueField' => 'label',
            'order' => 'label'
        ])->toArray();
        $journals = $this->Publications->Journals->find('list', [
            'keyField' => 'id',
            'valueField' => 'journal',
            'order' => 'journal'
        ])->toArray();
        $this->set(compact('publication', 'entryTypes', 'journals'));
    }

    /**
     * Edit method
     *
     * @param string|null $id Publication id.
     * @return \Cake\Http\Response|null Redirects on successful edit, renders view otherwise.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function edit($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $publication = $this->Publications->get($id, [
            'contain' => ['Authors', 'Editors']
        ]);
        if ($this->request->is(['patch', 'post', 'put'])) {
            $publication = $this->Publications->patchEntity($publication, $this->request->getData());
            if ($this->Publications->save($publication)) {
                $this->Flash->success(__('The publication has been saved.'));

                return $this->redirect(['action' => 'index']);
            }
            $this->Flash->error(__('The publication could not be saved. Please, try again.'));
        }
        $entryTypes = $this->Publications->EntryTypes->find('list', [
            'keyField' => 'id',
            'valueField' => 'label',
            'order' => 'label'
        ])->toArray();
        $journals = $this->Publications->Journals->find('list', [
            'keyField' => 'id',
            'valueField' => 'journal',
            'order' => 'journal'
        ])->toArray();
        $this->set(compact('publication', 'entryTypes', 'journals'));
    }

    /**
     * Delete method
     *
     * @param string|null $id Publication id.
     * @return \Cake\Http\Response|null Redirects to index.
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function delete($id = null)
    {
        // Access Check
        if (!$this->GeneralFunctions->checkIfRolesExists([1])) {
            $this->Flash->error($this->Auth->config('authError'));
            return $this->redirect($this->referer());
        }

        $this->request->allowMethod(['post', 'delete']);
        $publication = $this->Publications->get($id);
        if ($this->Publications->delete($publication)) {
            $this->Flash->success(__('The publication has been deleted.'));
        } else {
            $this->Flash->error(__('The publication could not be deleted. Please, try again.'));
        }

        return $this->redirect(['action' => 'index']);
    }

    public function autocomplete()
    {
        $this->autoRender = false;
        $terms = $this->Publications->find('list', [
            'conditions' => [
                        'bibtexkey LIKE' => trim($this->request->query['term']) . '%'
                ],
                'valueField' => ['bibtexkey']
            ])->toArray();
        echo json_encode($terms);
    }
    
    /**
     * Export method for downloading the entries containing errors.
     *
     * @param array $error_list_serialize Serialized list of all the objects containing errors.
     * @param array $header_serialize Serialized list of headers for the file.
     */
    public function export($error_list_serialize, $header_serialize)
    {
        $this->loadComponent('BulkUpload', ['table' => 'ArtifactsPublications']);
        $this->BulkUpload->export(unserialize($error_list_serialize), unserialize($header_serialize));
    }
}
