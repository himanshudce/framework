<?php
namespace App\Controller;

use App\Controller\AppController;

class SearchSettingsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();

        // Set access for public.
        $this->Auth->allow(['index', 'reset']);
    }

    /**
     * beforeFilter method
     *
     * To set up access before this contoller is executed.
     *
     * @return \Cake\Http\Response|void
     */
    public function beforeFilter(\Cake\Event\Event $event)
    {
        parent::beforeFilter($event);

        // Search Setting Form options
        $this->SearchSettingOption = [
            'PageSize' => [
                '10' => 10,
                '25' => 25,
                '100' => 100
            ],
            'Filter' => [
                'Sidebar' => [
                    '1' => 'Show filter sidebar',
                    '0' => 'Hide filter sidebar'
                ],
                'Object' => [
                    'museum_collection' => 'Museum Collections',
                    'period' => 'Period',
                    'provenience' => 'Provenience',
                    'object_type' => 'Object type',
                    'material' => 'Material'
                ],
                'Textual' => [
                    'genre-sub-genre' => 'Genre/sub-genre',
                    'language' => 'Language'
                ],
                'Publication' => [
                    'authors' => 'Authors',
                    'dop' => 'Date of publication'
                ]
            ]
        ];

        // Fetch Search Settings from Session Variable
        $this->searchSettings = $this->getRequest()->getSession()->read('searchSettings');
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        if ($this->request->is('post')) {
            if (isset($this->request->data['Reset'])) {
                $this->loadComponent('GeneralFunctions')->initializeSearchSettings();
                $this->searchSettings = $this->getRequest()->getSession()->read('searchSettings');
            } else {
                $this->searchSettings = $this->request->data;
                $this->getRequest()->getSession()->write('searchSettings', $this->searchSettings);
            }
        }
        $this->set([
            'searchSettings' => $this->searchSettings,
            'SearchSettingOption' => $this->SearchSettingOption
            ]);
    }
}
