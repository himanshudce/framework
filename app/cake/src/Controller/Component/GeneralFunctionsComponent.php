<?php
namespace App\Controller\Component;

use Cake\Controller\Component;
use Cake\ORM\TableRegistry;

class GeneralFunctionsComponent extends Component
{
    public $components = ['Auth'];

    /**
     * Check if role exists for the given user
     *
     * @param : $rolesToBeChecked
     * rolesToBeChecked -> array of roles to be checked
     *
     * @return : true or false
     */
    public function checkIfRolesExists($rolesToBeChecked)
    {
        $roles = $this->Auth->user('roles');

        if (!is_null($roles)) {
            $checkIfRoleExistsForAccess = array_intersect($roles, $rolesToBeChecked);

            if (!empty($checkIfRoleExistsForAccess)) {
                return true;
            }
        }

        return false;
    }

    /**
     * User Role
     *
     * @param : $userId
     * userId -> User ID
     *
     * @return : array of roles
     */
    public function getUsersRole($userId)
    {
        $this->RolesUsers = TableRegistry::get('RolesUsers');

        // GET User Role
        $rolesQuery = $this->RolesUsers->findByUserId($userId);

        $roles = [];

        foreach ($rolesQuery as $roleUser => $individualRole) {
            array_push($roles, $individualRole['role_id']);
        }
        return $roles;
    }

    /**
     * User Role
     *
     * @param : $userId
     * userId -> User ID
     *
     * @return : array of roles
     */
    public function initializeSearchSettings()
    {
        $this->searchSettings = [
            'PageSize' => '10',
            'filter:object' => [
                'museum_collection', 'period', 'provenience', 'object_type', 'material'
            ],
            'filter:textual' => [],
            'filter:publication' => [
                'authors', 'dop'
            ],
            'view:compact:filter:sidebar' => '1',
            'view:compact:filter:object' => [
                'museum_collection', 'period', 'provenience', 'object_type', 'material'
            ],
            'view:compact:filter:textual' => [],
            'view:compact:filter:publication' => [
                'authors', 'dop'
            ],
            'view:full:filter:sidebar' => '1',
            'view:full:filter:object' => [
                'museum_collection', 'period', 'provenience', 'object_type', 'material'
            ],
            'view:full:filter:textual' => [],
            'view:full:filter:publication' => [
                'authors', 'dop'
            ]
        ];
        $this->request->getSession()->write('searchSettings', $this->searchSettings);
    }
}
