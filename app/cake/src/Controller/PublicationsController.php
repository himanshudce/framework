<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;

/**
 * Publications Controller
 *
 * @property \App\Model\Table\PublicationsTable $Publications
 *
 * @method \App\Model\Entity\Publication[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class PublicationsController extends AppController
{
    /**
     * intialize method
     *
     * @return \Cake\Http\Response|void
     */
    public function initialize()
    {
        parent::initialize();
        $this->loadComponent('RequestHandler');
        $this->loadComponent('Bibliography');
        $this->loadComponent('TableExport');
        $this->loadComponent('LinkedData');

        // Set access for public.
        $this->Auth->allow(['index', 'view']);
    }

    /**
     * Index method
     *
     * @return \Cake\Http\Response|void
     */
    public function index()
    {
        $this->paginate = [
            'contain' => ['EntryTypes', 'Journals', 'Abbreviations', 'Authors', 'Editors']
        ];
        $publications = $this->paginate($this->Publications);

        $this->set(compact('publications'));
        $this->set('_serialize', 'publications');
    }

    /**
     * View method
     *
     * @param string|null $id Publication id.
     * @return \Cake\Http\Response|void
     * @throws \Cake\Datasource\Exception\RecordNotFoundException When record not found.
     */
    public function view($id = null)
    {
        $publication = $this->Publications->get($id, [
            'contain' => ['EntryTypes', 'Journals', 'Abbreviations', 'Authors', 'Editors']
        ]);

        $this->set('publication', $publication);
        $this->set('_serialize', 'publication');
    }
}
