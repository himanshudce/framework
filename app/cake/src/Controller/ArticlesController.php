<?php
namespace App\Controller;

use App\Controller\AppController;
use Cake\ORM\TableRegistry;
use Cake\Datasource\ConnectionManager;
use Cake\Http\Exception\ForbiddenException;

/**
 * Articles Controller
 *
 * @property \App\Model\Table\JournalsTable $Journals
 *
 * @method \App\Model\Entity\Journal[]|\Cake\Datasource\ResultSetInterface paginate($object = null, array $settings = [])
 */
class ArticlesController extends AppController
{
    public function initialize()
    {
        parent::initialize();
        $this->loadModel('Articles');
        $this->loadModel('ArticlesAuthors');
        $this->loadModel('Authors');
    }

    /**
     * indexCdln method
     */
    public function indexCdln()
    {
        $connection = ConnectionManager::get('default');
        
        $articles = $connection->execute("select * , articles.id as article_id,GROUP_CONCAT(authors.author) as authors from articles join articles_authors, authors where articles.article_type='cdln'
        and articles.id = articles_authors.article_id and articles_authors.author_id = authors.id group by articles.id; ")->fetchAll('assoc');

        $this->set(['cdln' => $articles ]);
    }

    /**
     * indexCdlp method
     */
    public function indexCdlp()
    {
        $connection = ConnectionManager::get('default');
        
        $articles = $connection->execute("select * , articles.id as article_id,GROUP_CONCAT(authors.author) as authors from articles join articles_authors, authors where articles.article_type='cdlp'
        and articles.id = articles_authors.article_id and articles_authors.author_id = authors.id group by articles.id; ")->fetchAll('assoc');

        $this->set(['cdlp' => $articles ]);
    }
    
    /**
     * viewCdln method
     */
    public function viewCdln()
    {
        $this->autoRender = false;

        $article = $this->request->params["cdln"];
        $article =  $this->Articles->find()->where(['id' => $article, 'article_type' => 'cdln'])->first();

        if ($article) {
            $this->response->file(
                'webroot/pubs/cdln/'.$article->pdf_link
            );
        } else {
            throw new ForbiddenException(__('No such CDLN article.'));
        }
    }

    /**
     * viewCdln method
     */
    public function viewCdlnWeb()
    {
        $this->autoRender = false;

        $article = $this->request->params["cdln"];
        $article =  $this->Articles->find()->where(['id' => $article, 'article_type' => 'cdln'])->first();

        if ($article) {
            $this->layout= '';
            $this->set(['cdln' => $article ]);
            $this->render('cdln_web_template');
        } else {
            throw new ForbiddenException(__('No such CDLN article.'));
        }
    }

    /**
     * viewCdlp method
     */
    public function viewCdlp()
    {
        $this->autoRender = false;
        $article = $this->request->params["cdlp"];
        $article =  $this->Articles->find()->where(['id' => $article, 'article_type' => 'cdlp'])->first();
        if ($article) {
            $this->response->file(
                'webroot/pubs/cdlp/'.$article->pdf_link
            );
        } else {
            throw new ForbiddenException(__('No such CDLP article.'));
        }
    }
}
