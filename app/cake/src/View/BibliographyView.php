<?php
namespace App\View;

use Cake\Event\EventManager;
use Cake\Http\Response;
use Cake\Http\ServerRequest;
use Cake\View\SerializedView;

class BibliographyView extends SerializedView
{

    /**
     * Map response types to Citation.js output types.
     *
     * @var array
     */
    protected static $formatsMap = [
        'bib' => 'bibtex',
        'biblatex' => 'biblatex',
        'bibliography' => 'bibliography',
        'csljson' => 'data',
        'ris' => 'ris'
    ];

    /**
     * @var string
     */
    protected static $defaultFormat = 'bibliography';

    /**
     * @var array
     */
    protected static $defaultOptions = [
        'format' => 'text',
        'template' => 'chicago'
    ];

    /**
     * List of special view vars.
     *
     * @var array
     */
    protected $_specialVars = ['_serialize', '_format', '_options'];

    /**
     * @var string
     */
    protected $_responseType = 'bibliography';

    /**
     * Constructor
     *
     * @param \Cake\Http\ServerRequest|null $request Request instance.
     * @param \Cake\Http\Response|null $response Response instance.
     * @param \Cake\Event\EventManager|null $eventManager EventManager instance.
     * @param array $viewOptions An array of view options
     */
    public function __construct(
        ServerRequest $request = null,
        Response $response = null,
        EventManager $eventManager = null,
        array $viewOptions = []
    ) {
        parent::__construct($request, $response, $eventManager, $viewOptions);
        $response = $response->withType($this->_getFormat());
    }

    public function initialize()
    {
        parent::initialize();
        $this->loadHelper('Scripts');
    }

    protected function _serialize($serialize)
    {
        return $this->Scripts->formatReference(
            $this->_dataToSerialize($serialize),
            $this->_formatToSerialize(),
            $this->_optionsToSerialize()
        );
    }

    /**
     * Returns data to be serialized.
     *
     * @param array|string|bool $serialize The name(s) of the view variable(s) that
     *   need(s) to be serialized. If true all available view variables will be used.
     * @return array The data to serialize.
     */
    protected function _dataToSerialize($serialize = true)
    {
        if ($serialize === true) {
            $data = array_diff_key(
                $this->viewVars,
                array_flip($this->_specialVars)
            );

            if (empty($data)) {
                return [];
            }

            return $data;
        }

        if (is_array($serialize)) {
            $data = [];
            foreach ($serialize as $alias => $key) {
                if (array_key_exists($key, $this->viewVars)) {
                    $data[$alias] = $this->viewVars[$key];
                }
            }

            return !empty($data) ? $data : [];
        }

        return isset($this->viewVars[$serialize]) ? [$this->viewVars[$serialize]] : [];
    }

    /**
     * @param string $format The format name
     * @return string The Citation.js output type
     */
    protected function _formatToSerialize()
    {
        return self::$formatsMap[$this->_getFormat()];
    }

    /**
     * @param string $format The format name
     * @return string
     */
    protected function _getFormat()
    {
        $format = $this->viewVars['_format'];

        if (empty($format) || empty(self::$formatsMap[$format])) {
            return self::$defaultFormat;
        }

        return $format;
    }

    /**
     * @param array $options
     * @return array
     */
    protected function _optionsToSerialize($options = [])
    {
        $options = $this->viewVars['_options'];
        return array_merge(self::$defaultOptions, $options);
    }
}
