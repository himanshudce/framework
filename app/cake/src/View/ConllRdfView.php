<?php
namespace App\View;

use App\Model\Entity\Inscription;
use Cake\View\SerializedView;

class ConllRdfView extends SerializedView
{
    use LinkedDataTrait;

    /**
     * List of special view vars.
     *
     * @var array
     */
    protected $_specialVars = ['_serialize'];

    /**
     * @var string
     */
    protected $_responseType = 'conll-rdf';

    public function initialize()
    {
        parent::initialize();
        $this->loadHelper('Scripts');
    }

    protected function _serialize($serialize)
    {
        $inscription = $this->_dataToSerialize($serialize);

        if (is_array($inscription)) {
            $inscription = $inscription[0];
        }

        if ($inscription instanceof Inscription) {
            return $this->Scripts->formatConllRdf(
                $inscription,
                [
                    'base' => self::$_base . $inscription->getUri() . '#'
                ]
            );
        } else {
            return null;
        }
    }
}
