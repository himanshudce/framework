<?php
namespace App\View\Cell;

use Cake\View\Cell;

class PublicationViewCell extends Cell
{
    public function display($id)
    {
        $this->loadModel('Publications');
        $publication = $this->Publications->get($id);

        $this->set(compact('publication'));
    }
}
