<?php
namespace App\View\Cell;

use Cake\View\Cell;

class ArtifactViewCell extends Cell
{
    public function display($id)
    {
        $this->loadModel('Artifacts');
        $artifact = $this->Artifacts->get($id, [
            'contain' => [
                'Proveniences',
                'Periods',
                'ArtifactTypes',
                'Archives',
                'Collections',
                'Dates',
                'ExternalResources',
                'Genres',
                'Languages',
                'Materials',
                'Publications',
                'Publications.Authors',
                'Publications.EntryTypes',
                'Publications.Journals',
                'ArtifactsComposites',
                'ArtifactsSeals',
                'ArtifactsShadow',
                'Inscriptions',
                'RetiredArtifacts'
            ]
        ]);

        $this->set(compact('artifact'));
    }
}
