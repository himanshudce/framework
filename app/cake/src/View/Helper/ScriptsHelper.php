<?php
namespace App\View\Helper;

use Cake\Cache\Cache;
use Cake\Http\Client;
use Cake\Http\Exception\HttpException;
use Cake\View\Helper;

class ScriptsHelper extends Helper
{
    private function _post($url, $data)
    {
        $http = new Client();
        $response = $http->post(
            $url,
            json_encode($data),
            ['type' => 'json']
        );

        $status = $response->getStatusCode();
        if ($status >= 400) {
            throw new HttpException($response->body, $status);
        }

        return $response->body;
    }

    /**
     * Convert data via an external script.
     *
     * @param string $url
     * @param array $data
     * @param string [$key]
     * @return string
     */
    private function post($url, $data, $key = null)
    {
        if (empty($key)) {
            return $this->_post($url, $data);
        } else {
            return Cache::remember($key, function () use ($url, $data) {
                return $this->_post($url, $data);
            });
        }
    }

    /**
     * Format any data into a reference.
     *
     * Option and format documentation: see /app/tools/citation.js/README.md.
     *
     * @param array $data
     * @param string $format
     * @param array $options
     * @return string
     */
    public function formatReference($data, $format, $options)
    {
        $url = sprintf('http://node-tools:3000/format/' . $format . '?' . http_build_query($options));
        return $this->post($url, $data);
    }

    /**
     * Get CoNLL-RDF from an inscription.
     *
     * Option array may contain the base URL.
     *
     * @param array $data
     * @param array $options
     * @return string
     */
    public function formatConllRdf($data, $options)
    {
        $url = 'http://node-tools:3001/format/rdf?' . http_build_query($options);
        return $this->post($url, $data, 'conll-rdf.' . $data->id);
    }

    /**
     * Get CoNLL-U from an inscription.
     *
     * @param array $data
     * @return string
     */
    public function formatConllU($data)
    {
        $url = 'http://node-tools:3002/format/u';
        return $this->post($url, $data, 'conll-u.' . $data->id);
    }
}
