<?php
namespace App\View;

use Cake\ORM\Entity;
use EasyRdf\Graph;
use EasyRdf\RdfNamespace;

trait LinkedDataTrait
{
    use SerializeTrait;

    protected static $_base = 'https://cdli.ucla.edu/';

    protected static $_context = [
        'cdli' => 'https://cdli.ucla.edu/docs/vocab/0.1#',
        'crm' => 'http://www.cidoc-crm.org/cidoc-crm/',
        'crma' => 'http://www.ics.forth.gr/isl/CRMarchaeo/',
        'bibtex' => 'http://purl.org/net/nknouf/ns/bibtex#',
        'rdfs' => 'http://www.w3.org/2000/01/rdf-schema#',
        'geo' => 'http://www.w3.org/2003/01/geo/wgs84_pos#',
        'osgeo' => 'http://data.ordnancesurvey.co.uk/ontology/geometry/'
    ];

    protected function prepareDataExport($data)
    {
        foreach (self::$_context as $prefix => $uri) {
            RdfNamespace::set($prefix, $uri);
        }

        $graph = new Graph();
        $graph->parse(
            json_encode($this->prepareJsonLd($data)),
            'jsonld'
        );

        return $graph;
    }

    protected function prepareJsonLd($data)
    {
        return [
            '@graph' => $this->makeJsonLd($data),
            '@context' => array_merge(
                [ '@base' => self::$_base],
                self::$_context
            )
        ];
    }

    protected function makeJsonLd($data)
    {
        if ($data instanceof Entity) {
            return $data->getCidocCrm();
        }

        if (is_array($data)) {
            foreach ($data as $key => $item) {
                if (is_array($item) || is_object($item)) {
                    $data[$key] = $this->makeJsonLd($item);
                } elseif (empty($item)) {
                    unset($data[$key]);
                }
            }
        }

        return $data;
    }
}
