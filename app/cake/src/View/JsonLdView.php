<?php
namespace App\View;

use Cake\View\JsonView as CakeJsonView;

class JsonLdView extends CakeJsonView
{
    use LinkedDataTrait;

    protected $_responseType = 'jsonld';

    protected function _serialize($serialize)
    {
        $data = $this->_dataToSerialize($serialize);
        $this->set('data', $this->prepareJsonLd($data));

        return parent::_serialize('data');
    }
}
