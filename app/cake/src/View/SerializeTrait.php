<?php
namespace App\View;

trait SerializeTrait
{
    /**
     * Returns data to be serialized.
     *
     * @param array|string|bool $serialize The name(s) of the view variable(s) that
     *   need(s) to be serialized. If true all available view variables will be used.
     * @param bool $keepAlias Keep aliases of viewVars
     * @return mixed The data to serialize.
     */
    protected function _dataToSerialize($serialize = true)
    {
        if ($serialize === true) {
            $data = array_diff_key(
                $this->viewVars,
                array_flip($this->_specialVars)
            );

            if (empty($data)) {
                return null;
            }

            return $data;
        }

        if (is_array($serialize)) {
            $data = [];
            foreach ($serialize as $key) {
                if (array_key_exists($key, $this->viewVars)) {
                    $data[] = $this->viewVars[$key];
                }
            }

            return !empty($data) ? $data : null;
        }

        if (isset($this->viewVars[$serialize])) {
            $data = $this->viewVars[$serialize];

            if ($data instanceof ResultSet) {
                $data = iterator_to_array($data);
            }

            return $data;
        }

        return ;
    }
}
