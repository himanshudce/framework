<?php
namespace App\Model\Entity;

use Cake\ORM\Entity;

/**
 * Inscription Entity
 *
 * @property int $id
 * @property int|null $artifact_id
 * @property string $atf
 * @property string $jtf
 * @property string|null $transliteration
 * @property string|null $transliteration_clean
 * @property string|null $tranliteration_sign_names
 * @property string|null $annotation
 * @property bool|null $is_atf2conll_diff_resolved
 * @property string|null $comments
 * @property string|null $structure
 * @property string|null $translation
 * @property string|null $transcription
 * @property int $accepted_by
 * @property bool $accepted
 * @property int $update_events_id
 * @property string $inscription_comments
 * @property bool $is_latest
 *
 * @property \App\Model\Entity\Artifact $artifact
 * @property \App\Model\Entity\UpdateEvent $update_event
 */
class Inscription extends Entity
{
    use LinkedDataTrait;

    /**
     * Fields that can be mass assigned using newEntity() or patchEntity().
     *
     * Note that when '*' is set to true, this allows all unspecified fields to
     * be mass assigned. For security purposes, it is advised to set '*' to false
     * (or remove it), and explicitly make individual fields accessible as needed.
     *
     * @var array
     */
    protected $_accessible = [
        'artifact_id' => true,
        'atf' => true,
        'jtf' => true,
        'transliteration' => true,
        'transliteration_clean' => true,
        'tranliteration_sign_names' => true,
        'annotation' => true,
        'is_atf2conll_diff_resolved' => true,
        'comments' => true,
        'structure' => true,
        'translation' => true,
        'transcription' => true,
        'accepted_by' => true,
        'accepted' => true,
        'update_events_id' => true,
        'inscription_comments' => true,
        'is_latest' => true,
        'artifact' => true,
        'update_event' => true
    ];

    public function getCidocCrm($artifact = null)
    {
        $inscription = [
            '@id' => $this->getUri(),
            '@type' => 'crm:E34_Inscription',
            'crm:P3_has_note' => $this->atf
            // TODO
        ];

        if (empty($artifact) && !empty($this->artifact)) {
            $inscription['crm:P128i_is_carried_by'] = $this->artifact->getCidocCrm();
            $artifact = $this->artifact;
        } else {
            // TODO $inscription['crm:P128i_is_carried_by'] = $this->getUri();
        }

        if (!empty($artifact)) {
            $inscription['crm:P72_has_language'] = self::getEntities($artifact->languages);
            $inscription['crm:P148i_is_component_of'] = self::getEntities($artifact->composites);
            $inscription['crm:P67_refers_to'] = self::getEntities($artifact->dates);
        }

        return $inscription;
    }
}
